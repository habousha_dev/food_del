import {AsyncStorage} from '@react-native-community/async-storage';
import Reactotron from 'reactotron-react-native';
import {reactotronRedux as reduxPlugin} from 'reactotron-redux';

console.disableYellowBox = true;
const reactotron = Reactotron;
// First, set some configuration settings on how to connect to the app
reactotron.setAsyncStorageHandler(AsyncStorage);
reactotron.configure({
  name: 'Demo App',
});

reactotron.useReactNative({
  asyncStorage: {ignore: ['secret']},
});

// add some more plugins for redux & redux-saga
reactotron.use(reduxPlugin());

// if we're running in DEV mode, then let's connect!
if (__DEV__) {
  reactotron.connect();
  reactotron.clear();
}

reactotron.onCustomCommand('test', () => console.log('This is an example'));
export default reactotron;
// console.tron = Reactotron
