/****************************************************************************************/
/********************************IMPORT SCREENS***********************************************/
/****************************************************************************************/
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import AddNewAddressScreen from '../screens/AddNewAddress';
import AddressBookScreen from '../screens/AddressBook';
import BranchesScreen from '../screens/Branches';
import CartScreen from '../screens/Cart';
import CheckoutScreen from '../screens/Checkout';
import EditAccountScreen from '../screens/EditAccount';
import FavoritesScreen from '../screens/Favorites';
import ForgetPasswordScreen from '../screens/Forget';
import HelpScreen from '../screens/Help';
import HomeScreen from '../screens/Home';
import MyAccountScreen from '../screens/MyAccount';
import NotificationScreen from '../screens/Notifications';
import OffersScreen from '../screens/Offers';
import OrdersScreen from '../screens/Orders';
import OrderStatusScreen from '../screens/OrderStatus';
import PrivacyScreen from '../screens/PrivacyPolicy';
import SignInScreen from '../screens/Signin';
import SignupScreen from '../screens/Signup';
import StatusScreen from '../screens/Status';
import WalkthroughScreen from '../screens/Walkthrough';
/****************************************************************************************/
/****************************************************************************************/

const defaultNavOptions = {
  headerShown: false,
};

const HomeStack = createStackNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: defaultNavOptions,
  },
  Walkthrough: {
    screen: WalkthroughScreen,
    navigationOptions: defaultNavOptions,
  },
  Signup: {
    screen: SignupScreen,
    navigationOptions: defaultNavOptions,
  },
  SignIn: {
    screen: SignInScreen,
    navigationOptions: defaultNavOptions,
  },
  Forget: {
    screen: ForgetPasswordScreen,
    navigationOptions: defaultNavOptions,
  },
  AddressBook: {
    screen: AddressBookScreen,
    navigationOptions: defaultNavOptions,
  },
  AddNewAddress: {
    screen: AddNewAddressScreen,
    navigationOptions: defaultNavOptions,
  },
  OrderStatus: {
    screen: OrderStatusScreen,
    navigationOptions: defaultNavOptions,
  },
  Favorites: {
    screen: FavoritesScreen,
    navigationOptions: defaultNavOptions,
  },
  Cart: {
    screen: CartScreen,
    navigationOptions: defaultNavOptions,
  },
  Notification: {
    screen: NotificationScreen,
    navigationOptions: defaultNavOptions,
  },

  MyAccount: {
    screen: MyAccountScreen,
    navigationOptions: defaultNavOptions,
  },
  Orders: {
    screen: OrdersScreen,
    navigationOptions: defaultNavOptions,
  },
  Offers: {
    screen: OffersScreen,
    navigationOptions: defaultNavOptions,
  },
  Branches: {
    screen: BranchesScreen,
    navigationOptions: defaultNavOptions,
  },

  Checkout: {
    screen: CheckoutScreen,
    navigationOptions: defaultNavOptions,
  },

  Help: {
    screen: HelpScreen,
    navigationOptions: defaultNavOptions,
  },

  EditAccount: {
    screen: EditAccountScreen,
    navigationOptions: defaultNavOptions,
  },

  Status: {
    screen: StatusScreen,
    navigationOptions: defaultNavOptions,
  },

  Privacy: {
    screen: PrivacyScreen,
    navigationOptions: defaultNavOptions,
  },
});

export default createAppContainer(HomeStack);
