import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import normalize from 'react-native-normalize';

class ButtonComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }

  render() {
    let {btnText} = this.props;
    return (
      <TouchableOpacity
        {...this.props}
        style={{
          // width: '100%',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          alignSelf: 'center',
          // height: 500,
          // padding: 20,
        }}>
        <LinearGradient
          style={[styles.Gradient, this.props.sendStyle]}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          // locations={[0, 0.5, 0.6]}
          colors={['#a69038', '#c2ad58', '#c2ad58']}>
          <Text style={styles.ButtonText}>{btnText}</Text>
        </LinearGradient>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  Gradient: {
    height: normalize(50, 'height'),
    borderRadius: normalize(50, 'height') / 2,
    width: normalize(200, 'width'),
    // paddingHorizontal: normalize(20, 'width'),
    // width: width - 40,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    // padding: 10,
  },
  ButtonText: {
    textAlign: 'center',
    color: '#ffffff',
    fontSize: normalize(17, 'width'),
    fontFamily: 'sst_med',
    // fontWeight:'bold'
    // padding: 15,
    // height: 50,
    // display: 'flex',
    // alignItems: 'center',
    // justifyContent: 'center',
    // alignSelf: 'center',
    // elevation: 1,
    // width: '100%',

    // borderRadius: 100,
    // fontWeight: 'bold',
  },
});

export default ButtonComponent;
