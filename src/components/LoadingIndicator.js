import React, { Component } from 'react';
import { StatusBar, StyleSheet } from 'react-native';
import AnimatedLoader from 'react-native-animated-loader';

class LoadingIndicator extends Component {
  constructor(props) {
    super(props);
    this.state = {visible: true};
  }

  render() {
    return (
      <>
        <StatusBar backgroundColor="rgba(123,189,74,0.8)" />
        <AnimatedLoader
          visible={true}
          overlayColor="rgba(123,189,74,0.8)"
          source={require('../../assets/loader.json')}
          animationStyle={styles.lottie}
          speed={1}
        />
        {/* {console.log(this.props)} */}
      </>
    );
  }
}

const styles = StyleSheet.create({
  lottie: {
    width: 400,
    height: 400,
    // color:red
  },
});

export default LoadingIndicator;
