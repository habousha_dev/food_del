import React, {useState} from 'react';
import {Keyboard, StyleSheet, TextInput, View} from 'react-native';

// const DismissKeyboard = ({children}) => (
//   <TouchableWithoutFeedback
//     onPress={() => Keyboard.dismiss()}
//     accessible={false}>
//     {children}
//   </TouchableWithoutFeedback>
// );

export function Input(props) {
  const [inputColor, setInputColor] = useState('red');
  const [iconColor, setIconColor] = useState('green');
  const {
    underlined,
    underlineColor,
    underlineHeight,
    bordered,
    borderColor,
    borderWidth,
    borderRadius,
    placeholderPosition,
    renderIconLeft,
    renderIconRight,
    stacked,
    placeholder,
    style,
    wrapperStyle,
    iconWrapperStyle,
    showValidation,
    isValid,
    touched,
    ...rest
  } = props;

  return (
    <View
      style={[
        bordered && {
          borderWidth: borderWidth || 1,
          borderColor: borderColor || '#bbb',
          // borderColor: borderColor || '#bbb',
          borderRadius: borderRadius || 5,
          padding: 5,
        },
        wrapperStyle,
      ]}>
      {stacked && <Text style={{color: '#bbb'}}>{placeholder}</Text>}

      <View style={{flexDirection: 'row'}}>
        {renderIconLeft && (
          <View style={[styles.iconWrapper, iconWrapperStyle]}>
            {renderIconLeft()}
          </View>
        )}
        <TextInput
          {...rest}
          onSubmitEditing={Keyboard.dismiss}
          // onFocus={() => { this.setState({ input1: '#45c9ae', input1Icon: '#45c9ae' }) }}
          // onBlur={() => { this.setState({ input1: '#ebebeb', input1Icon: '#a3a3a3' }) }}
          // onFocus={()=>{{setInputColor('#45c9ae'),setIconColor('#45c9ae')}}}
          // onBlur={()=>{{setInputColor('#45c9ae'),setIconColor('#45c9ae')}}}
          placeholder={stacked ? '' : placeholder}
          style={[
            {
              flex: 1,
              padding: 0,
              textAlign: placeholderPosition || 'left',
            },
            style,
          ]}
        />
        {renderIconRight && (
          <View style={[styles.iconWrapper, iconWrapperStyle]}>
            {renderIconRight()}
          </View>
        )}

        {/* {showValidation && touched && (
          <IonIcon
            style={{fontSize: 24, color: isValid ? 'green' : 'red'}}
            name={isValid ? 'checkmark' : 'close'}
          />
        )} */}
      </View>

      {(underlined || stacked) && (
        <View
          style={{
            height: underlineHeight || 1,
            backgroundColor: underlineColor || 'rgb(229, 229, 229)',
          }}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  iconWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
