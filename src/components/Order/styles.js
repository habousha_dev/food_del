/*************************************************************/

import {ScaledSheet} from 'react-native-size-matters';
import {MAIN_COLOR, TEXT_COLOR} from '../../constants/Colors';
import {FONTS} from '../../constants/Fonts';
/*************************************************************/

const {FrutigerLight, Frutigerroman, FrutigerBold, FrutigerBlack} = FONTS;
/*************************************************************/

const styles = ScaledSheet.create({
  wrraper: {
    paddingHorizontal: '20@s',
    paddingBottom: '15@s',
    // marginBottom: '15@s',
  },
  ordercontent: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: '15@s',
  },
  detailswrraper: {
    display: 'flex',
    flexDirection: 'row',
  },
  numwrraper: {
    display: 'flex',
    flexDirection: 'row',
  },
  ordernumber: {
    backgroundColor: '#f2f2f2',
    color: '#9d9d9d',
    fontSize: '10@s',
    fontFamily: FrutigerBold,
    paddingHorizontal: '10@s',
    paddingVertical: '4@s',
    marginHorizontal: '5@s',
    borderRadius: '15@s',
    marginTop: '25@s',
  },
  orderright: {
    backgroundColor: '#f0c632',
    color: '#fff',
    fontSize: '10@s',
    fontFamily: FrutigerBold,
    paddingHorizontal: '10@s',
    paddingVertical: '4@s',
    borderRadius: '15@s',
    marginTop: '25@s',
  },
  qty: {
    color: TEXT_COLOR,
    fontFamily: FrutigerBold,
    fontSize: '15@s',
  },
  name: {
    color: TEXT_COLOR,
    fontSize: '15@s',
    fontFamily: FrutigerBold,
    marginHorizontal: '15@s',
  },
  price: {
    color: MAIN_COLOR,
    fontSize: '15@s',
    fontFamily: FrutigerBold,
  },
  vatwrapper: {
    marginHorizontal: '40@s',
    marginTop: '5@s',
  },
  arrive: {
    fontSize: '13@s',
    fontFamily: FrutigerLight,
    color: TEXT_COLOR,
  },
  vat: {},
  orderdetailswrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderTopColor: '#e5e5e5',
    borderTopWidth: 1,
    paddingTop: '15@s',
    marginTop: '15@s',
  },
  orderdetailstext: {
    fontSize: '15@s',
    fontFamily: FrutigerBold,
    color: MAIN_COLOR,
  },
  orderdetailsicon: {
    fontSize: '15@s',
    color: MAIN_COLOR,
  },
  dummyborder: {
    backgroundColor: '#rgb(242, 242, 242)',
    height: '6@s',
  },
});
export default styles;
