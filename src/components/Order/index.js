import React, {useEffect} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import langs from '../../langs/ar';
import styles from './styles';

/*************************************************************/

/*************************************************************/

const Order = ({onPress, navigation, props}) => {
  useEffect(() => {
    return console.log('props fff', props);
  }, []);
  return (
    <View>
      <View style={styles.wrraper}>
        <View style={styles.numwrraper}>
          <Text style={[styles.orderright]}>{langs.ordernum} 2547</Text>
          <Text style={styles.ordernumber}>25 مارس 2019</Text>
        </View>
        <View style={styles.ordercontent}>
          <View style={styles.detailswrraper}>
            <View style={styles.qtywrraper}>
              <Text style={styles.qty}>x 2</Text>
              <Text style={styles.qty}>x 2</Text>
              <Text style={styles.qty}>x 2</Text>
              <Text style={styles.qty}>x 2</Text>
            </View>
            <View style={styles.namewrraper}>
              <Text style={styles.name}>وجبة فروج</Text>
              <Text style={styles.name}>وجبة فروج</Text>
              <Text style={styles.name}>وجبة فروج</Text>
              <Text style={styles.name}>وجبة فروج</Text>
            </View>
          </View>
          <View style={styles.pricewrraper}>
            <Text style={styles.price}>356 {langs.riyal}</Text>
          </View>
        </View>
        <View style={styles.vatwrapper}>
          <Text style={styles.arrive}>توصيل</Text>
          <Text style={styles.arrive}>{langs.vat}</Text>
        </View>
        <TouchableOpacity style={styles.orderdetailswrapper} onPress={onPress}>
          <Text style={styles.orderdetailstext}>{langs.orderdet}</Text>
          <Icon name="chevron-left" style={styles.orderdetailsicon} />
        </TouchableOpacity>
      </View>
      <View style={styles.dummyborder}></View>
    </View>
  );
};

export default Order;
