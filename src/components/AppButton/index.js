import React from 'react';
import {ActivityIndicator, Text} from 'react-native';
import {PlatformTouchable} from '../PlatformTouchable';
import styles from './styles';
export class AppButton extends React.Component {
  render() {
    const {
      title,
      wrapperStyle,
      titleStyle,
      isLoading,
      disabled,
      iconStyle,
      iconame,
      ...rest
    } = this.props;

    return (
      <PlatformTouchable
        {...rest}
        disabled={disabled || isLoading}
        style={[
          styles.wrapper,
          wrapperStyle,
          disabled ? styles.disabled : null,
        ]}>
        {isLoading ? (
          <ActivityIndicator />
        ) : (
          <Text style={[styles.title, titleStyle]}>{title}</Text>
        )}
      </PlatformTouchable>
    );
  }
}
