import {Dimensions} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {MAIN_COLOR} from '../../constants/Colors';
import {FONTS} from '../../constants/Fonts';

/*************************************************************/
const win = Dimensions.get('window');
const ratio = win.height / 2;

const {FrutigerLight, Frutigerroman, FrutigerBold, FrutigerBlack} = FONTS;
/*************************************************************/

const styles = ScaledSheet.create({
  wrapper: {
    backgroundColor: MAIN_COLOR,
    borderColor: MAIN_COLOR,
    borderRadius: ratio,
    borderWidth: 1,
    paddingVertical: '15@vs',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: '20@s',
    // flex: 1,
    // width: '100%',
    // height: 80,
  },
  title: {
    color: '#fff',
    fontFamily: FrutigerBold,
    fontSize: '15@s',
  },
  disabled: {
    opacity: 0.3,
  },
});

export default styles;
