import {Button, Footer, FooterTab, Text} from 'native-base';
import React, {Component} from 'react';
import {ScaledSheet} from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {withNavigation} from 'react-navigation';
import {FONTS} from '../constants/Fonts';
import langs from '../langs/ar';

/*************************************************************/

const {FrutigerLight, Frutigerroman, FrutigerBold, FrutigerBlack} = FONTS;
/*************************************************************/

class Footerdef extends Component {
  render() {
    return (
      <Footer style={styles.footer}>
        <FooterTab style={styles.footertab}>
          <Button
            onPress={() => {
              this.props.navigation.navigate('MyAccount');
            }}
            style={styles.title}>
            <Icon
              name="user"
              solid
              size={23}
              style={
                this.props.navigation.state.routeName === 'MyAccount'
                  ? {color: '#7bbd4a'}
                  : {color: '#bdbdbd'}
              }
            />
            <Text
              style={
                this.props.navigation.state.routeName === 'MyAccount'
                  ? {...styles.inactivetitle}
                  : {...styles.title}
              }>
              {langs.accheader}
            </Text>
          </Button>
        </FooterTab>

        <FooterTab style={styles.footertab}>
          <Button
            onPress={() => {
              this.props.navigation.navigate('Favorites');
            }}
            style={styles.title}>
            <Icon
              name="heart"
              solid
              size={23}
              style={
                this.props.navigation.state.routeName === 'Favorites'
                  ? {color: '#7bbd4a'}
                  : {color: '#bdbdbd'}
              }
            />
            <Text
              style={
                this.props.navigation.state.routeName === 'Favorites'
                  ? {...styles.inactivetitle}
                  : {...styles.title}
              }>
              {langs.favheader}
            </Text>
          </Button>
        </FooterTab>

        <FooterTab style={styles.footertab}>
          <Button
            onPress={() => this.props.navigation.navigate('Home')}
            style={styles.title}>
            <Icon
              name="home"
              solid
              size={23}
              style={
                this.props.navigation.state.routeName === 'Home'
                  ? {color: '#7bbd4a'}
                  : {color: '#bdbdbd'}
              }
            />
            <Text
              style={
                this.props.navigation.state.routeName === 'Home'
                  ? {...styles.inactivetitle}
                  : {...styles.title}
              }>
              {langs.home}
            </Text>
          </Button>
        </FooterTab>

        <FooterTab style={styles.footertab}>
          <Button
            onPress={() => this.props.navigation.navigate('Offers')}
            style={styles.title}>
            <Icon
              name="percent"
              solid
              size={23}
              style={
                this.props.navigation.state.routeName === 'Offers'
                  ? {color: '#7bbd4a'}
                  : {color: '#bdbdbd'}
              }
            />
            <Text
              style={
                this.props.navigation.state.routeName === 'Offers'
                  ? {...styles.inactivetitle}
                  : {...styles.title}
              }>
              {langs.offersheader}
            </Text>
          </Button>
        </FooterTab>

        <FooterTab active style={styles.footertab}>
          <Button
            onPress={() => this.props.navigation.navigate('Orders')}
            style={styles.title}>
            <Icon
              name="file-alt"
              solid
              size={23}
              style={
                this.props.navigation.state.routeName === 'Orders'
                  ? {color: '#7bbd4a'}
                  : {color: '#bdbdbd'}
              }
            />
            <Text
              style={
                this.props.navigation.state.routeName === 'Orders'
                  ? {...styles.inactivetitle}
                  : {...styles.title}
              }>
              {langs.ordersheader}
            </Text>
          </Button>
        </FooterTab>
      </Footer>
    );
  }
}

/*************************************************************/

const styles = ScaledSheet.create({
  footer: {
    backgroundColor: '#fff',
    height: '65@s',
    shadowOffset: {height: 0, width: 0},
    borderBottomWidth: 0,
    shadowOpacity: 0.75,
    elevation: 6,
    shadowColor: '#000000',
    shadowRadius: 5,
    borderColor: '#rgb(245, 245, 245)',
    borderWidth: 1,
    // flex: 1,
    // bottom: 0
  },
  footertab: {
    backgroundColor: '#ffffff',
    alignSelf: 'center',
    // borderLeftColor: '#e4e4e4',
    // borderLeftWidth: 1,
  },
  title: {
    color: '#bdbdbd',
    fontSize: '11@s',
    fontFamily: FrutigerBold,
    paddingTop: '10@vs',
  },
  inactivetitle: {
    color: '#53a045',
    fontSize: '11@s',
    fontFamily: FrutigerBold,
    // paddingTop: 5,
    paddingTop: '10@vs',
  },
});

export default withNavigation(Footerdef);
