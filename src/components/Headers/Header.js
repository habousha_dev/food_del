import {Body, Header, Right} from 'native-base';
import React, {Component} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {withNavigation} from 'react-navigation';
import {HEADER_COLOR} from '../../constants/Colors';
import {FONTS} from '../../constants/Fonts';

/*************************************************************/
const {FrutigerLight, Frutigerroman, FrutigerBold, FrutigerBlack} = FONTS;
/****************************************************************************************/

class HeaderCust extends Component {
  constructor(props) {
    super(props);
  }
  /****************************************************************************************/

  componentDidMount() {
    // console.log(this.props);
  }
  render() {
    let {title, navigation, style} = this.props;
    return (
      <View>
        <Header
          style={[styles.mainHead, style]}
          iosBarStyle={'light-content'}
          androidStatusBarColor={'#000'}>
          <Body style={styles.headerBody}>
            <Text style={styles.headerText} numberOfLines={1}>
              {title}
            </Text>
          </Body>
          <Right style={styles.HeaderRight}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}>
              <FontAwesome5 name="arrow-left" style={styles.icon} />
            </TouchableOpacity>
          </Right>
        </Header>
      </View>
    );
  }
}

const styles = ScaledSheet.create({
  mainHead: {
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    elevation: 8,
    height: '60@s',
  },
  headerBody: {
    flex: 8,
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  icon: {
    fontSize: '18@s',
    color: HEADER_COLOR,
  },
  headerText: {
    color: HEADER_COLOR,
    writingDirection: 'rtl',
    fontSize: '20@s',
    fontFamily: FrutigerBold,
  },
  HeaderRight: {
    flex: 1,
    display: 'flex',
  },
});

export default withNavigation(HeaderCust);
