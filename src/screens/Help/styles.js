/*************************************************************/

import {ScaledSheet} from 'react-native-size-matters';
import {TEXT_COLOR} from '../../constants/Colors';
import {FONTS} from '../../constants/Fonts';
/*************************************************************/

const {FrutigerLight, Frutigerroman, FrutigerBold, FrutigerBlack} = FONTS;
/*************************************************************/

const styles = ScaledSheet.create({
  conatiner: {
    flex: 1,
    backgroundColor: '#fff',
  },
  imagewrapper: {
    marginBottom: '10@vs',
  },
  headertitle: {
    fontSize: '13@s',
    color: TEXT_COLOR,
    fontFamily: FrutigerBold,
    paddingHorizontal: '5@s',
  },
  headericon: {
    fontSize: '20@s',
    color: '#7bbd4a',
  },
  headerwrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: '#rgb(246, 250, 242)',
    height: '60@vs',
    borderBottomColor: '#rgb(235, 235, 235)',
    borderBottomWidth: 1,
  },
  contertext: {
    fontSize: '15@s',
    color: TEXT_COLOR,
    fontFamily: FrutigerLight,
    textAlign: 'justify',
  },
  conterwrapper: {
    padding: '20@s',
  },
});
export default styles;
