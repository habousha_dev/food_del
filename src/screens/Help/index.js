import {Accordion, Container, Content} from 'native-base';
import React from 'react';
import {Dimensions, Image, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import HeaderCust from '../../components/Headers/Header';
import {Images} from '../../constants/Images';
import langs from '../../langs/ar';
import styles from './styles';
const win = Dimensions.get('window');
const ratio = win.width / 738; //738 is actual image width

/*************************************************************/

const dataArray = [
  {
    title: 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي',
    content:
      'لافاَ للإعتقاد السائد فإن لوريم إيبسوم ليس نصاَ عشوائياً، بل إن له جذور في الأدب اللاتيني الكلاسيكي منذ العام 45 قبل الميلاد، مما يجعله أكثر من 2000 عام في القدم. قام البروفيسور "ريتشارد ماك لينتوك" (Richard McClintock) وهو بروفيسور اللغة اللاتينية في جامعة هامبدن-سيدني في فيرجينيا بالبحث عن أصول كلمة لاتينية غامضة في نص لوريم إيبسوم وهي "consectetur"، وخلال تتبعه لهذه الكلمة في الأدب اللاتيني اكتشف المصدر الغير قابل للشك',
  },
  {
    title: 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي',
    content:
      'لافاَ للإعتقاد السائد فإن لوريم إيبسوم ليس نصاَ عشوائياً، بل إن له جذور في الأدب اللاتيني الكلاسيكي منذ العام 45 قبل الميلاد، مما يجعله أكثر من 2000 عام في القدم. قام البروفيسور "ريتشارد ماك لينتوك" (Richard McClintock) وهو بروفيسور اللغة اللاتينية في جامعة هامبدن-سيدني في فيرجينيا بالبحث عن أصول كلمة لاتينية غامضة في نص لوريم إيبسوم وهي "consectetur"، وخلال تتبعه لهذه الكلمة في الأدب اللاتيني اكتشف المصدر الغير قابل للشك',
  },
  {
    title: 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي',
    content:
      'لافاَ للإعتقاد السائد فإن لوريم إيبسوم ليس نصاَ عشوائياً، بل إن له جذور في الأدب اللاتيني الكلاسيكي منذ العام 45 قبل الميلاد، مما يجعله أكثر من 2000 عام في القدم. قام البروفيسور "ريتشارد ماك لينتوك" (Richard McClintock) وهو بروفيسور اللغة اللاتينية في جامعة هامبدن-سيدني في فيرجينيا بالبحث عن أصول كلمة لاتينية غامضة في نص لوريم إيبسوم وهي "consectetur"، وخلال تتبعه لهذه الكلمة في الأدب اللاتيني اكتشف المصدر الغير قابل للشك',
  },
];
/*************************************************************/
const _renderHeader = (item, expanded) => {
  return (
    <View
      style={{
        ...styles.headerwrapper,
      }}>
      {expanded ? (
        <Icon name="minus-circle" style={styles.headericon} />
      ) : (
        <Icon name="plus-circle" style={styles.headericon} />
      )}
      <Text style={styles.headertitle}> {item.title}</Text>
    </View>
  );
};
const _renderContent = (item) => {
  return (
    <View style={styles.conterwrapper}>
      <Text style={styles.contertext}>{item.content}</Text>
    </View>
  );
};

const HelpScreen = ({props}) => {
  return (
    <Container>
      <HeaderCust title={` ${langs.helpheader}`} style={styles.headfix} />
      <Content style={styles.conatiner}>
        <View style={styles.imagewrapper}>
          <Image
            source={Images.help_image}
            style={{width: win.width, height: 270 * ratio}}
          />
        </View>
        <Accordion
          dataArray={dataArray}
          expanded={0}
          animation={true}
          expanded={true}
          renderHeader={_renderHeader}
          renderContent={_renderContent}
          style={{borderColor: 'transparent'}}
        />
      </Content>
    </Container>
  );
};

export default HelpScreen;
