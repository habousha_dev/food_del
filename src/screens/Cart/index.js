import {Container, Content} from 'native-base';
import React from 'react';
import {Animated, Image, Text, TouchableOpacity, View} from 'react-native';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {AppButton} from '../../components/AppButton';
import HeaderCust from '../../components/Headers/Header';
import {Input} from '../../components/Input';
import {Images} from '../../constants/Images';
import langs from '../../langs/ar';
import styles from './styles';

/*************************************************************/

const renderProduct = () => {
  /*************************************************************/

  const _renderRightAction = (icon, color, backgroundColor, x, progress) => {
    const trans = progress.interpolate({
      inputRange: [0, 1],
      outputRange: [x, 0],
    });

    return (
      <Animated.View style={{flex: 1, transform: [{translateX: trans}]}}>
        <TouchableOpacity style={styles.icondeletewrapper}>
          <Icon name="trash-alt" solid style={styles.icondelete} />
        </TouchableOpacity>
      </Animated.View>
    );
  };

  const _renderRightActions = (progress) => (
    <View style={{width: 100, flexDirection: 'row'}}>
      {_renderRightAction(
        'trash',
        '#ffffff',
        '#rgb(255, 65, 65)',
        64,
        progress,
      )}
    </View>
  );

  _updateRef = (ref) => {
    _swipeableRow = ref;
  };
  /*************************************************************/

  return (
    <View style={styles.prodswrapper}>
      <View style={styles.stock}>
        <Text style={styles.stocktext}>{langs.stockhint}</Text>
        <Icon name="exclamation-circle" style={styles.stockicon} />
      </View>
      <Swipeable
        ref={_updateRef}
        friction={2}
        rightThreshold={40}
        renderRightActions={_renderRightActions}>
        <TouchableOpacity style={styles.prodwrapper}>
          <View style={styles.prodcontainer}>
            <View style={styles.imagecontainer}>
              <Image source={Images.cart_icon} style={styles.image} />
            </View>
            <View style={styles.titlecontainer}>
              <View>
                <Text style={styles.text}>وجبة فروج</Text>
              </View>
              <View style={styles.countwrapper}>
                <View>
                  <Icon name="minus" style={styles.icon} />
                </View>
                <View style={styles.numberwrapper}>
                  <Text style={styles.number}>2</Text>
                </View>
                <View>
                  <Icon name="plus" style={styles.icon} />
                </View>
              </View>
            </View>
          </View>
          <View style={styles.price}>
            <Text style={styles.pricetext}>35 {langs.riyal}</Text>
          </View>
        </TouchableOpacity>
      </Swipeable>
      <Swipeable
        ref={_updateRef}
        friction={2}
        rightThreshold={40}
        renderRightActions={_renderRightActions}>
        <TouchableOpacity style={styles.prodwrapper}>
          <View style={styles.prodcontainer}>
            <View style={styles.imagecontainer}>
              <Image source={Images.cart_icon} style={styles.image} />
            </View>
            <View style={styles.titlecontainer}>
              <View>
                <Text style={styles.text}>وجبة فروج</Text>
              </View>
              <View style={styles.countwrapper}>
                <View>
                  <Icon name="minus" style={styles.icon} />
                </View>
                <View style={styles.numberwrapper}>
                  <Text style={styles.number}>2</Text>
                </View>
                <View>
                  <Icon name="plus" style={styles.icon} />
                </View>
              </View>
            </View>
          </View>
          <View style={styles.price}>
            <Text style={styles.pricetext}>35 {langs.riyal}</Text>
          </View>
        </TouchableOpacity>
      </Swipeable>
      <Swipeable
        ref={_updateRef}
        friction={2}
        rightThreshold={40}
        renderRightActions={_renderRightActions}>
        <TouchableOpacity style={styles.prodwrapper}>
          <View style={styles.prodcontainer}>
            <View style={styles.imagecontainer}>
              <Image source={Images.cart_icon} style={styles.image} />
            </View>
            <View style={styles.titlecontainer}>
              <View>
                <Text style={styles.text}>وجبة فروج</Text>
              </View>
              <View style={styles.countwrapper}>
                <View>
                  <Icon name="minus" style={styles.icon} />
                </View>
                <View style={styles.numberwrapper}>
                  <Text style={styles.number}>2</Text>
                </View>
                <View>
                  <Icon name="plus" style={styles.icon} />
                </View>
              </View>
            </View>
          </View>
          <View style={styles.price}>
            <Text style={styles.pricetext}>35 {langs.riyal}</Text>
          </View>
        </TouchableOpacity>
      </Swipeable>
      <Swipeable
        ref={_updateRef}
        friction={2}
        rightThreshold={40}
        renderRightActions={_renderRightActions}>
        <TouchableOpacity style={styles.prodwrapper}>
          <View style={styles.prodcontainer}>
            <View style={styles.imagecontainer}>
              <Image source={Images.cart_icon} style={styles.image} />
            </View>
            <View style={styles.titlecontainer}>
              <View>
                <Text style={styles.text}>وجبة فروج</Text>
              </View>
              <View style={styles.countwrapper}>
                <View>
                  <Icon name="minus" style={styles.icon} />
                </View>
                <View style={styles.numberwrapper}>
                  <Text style={styles.number}>2</Text>
                </View>
                <View>
                  <Icon name="plus" style={styles.icon} />
                </View>
              </View>
            </View>
          </View>
          <View style={styles.price}>
            <Text style={styles.pricetext}>35 {langs.riyal}</Text>
          </View>
        </TouchableOpacity>
      </Swipeable>
    </View>
  );
};

function renderIconRight() {
  return <Icon name="check" style={styles.iconleft} />;
}

const CartScreen = ({navigation}) => {
  return (
    <Container>
      <HeaderCust title={`${langs.cartheader}`} />
      <Content style={styles.conatiner}>
        {renderProduct()}

        <View style={styles.inputwrapper}>
          <Text style={styles.coupon}>اضف كوبون التخفيض</Text>
          <Input
            placeholder="اكتب كود كوبون التخفيض هنا"
            underlined
            placeholderPosition="right"
            renderIconRight={renderIconRight}
            style={styles.input}
          />
        </View>
        <View style={styles.btmcontentValues}>
          <View style={styles.descwrapper}>
            <Text style={styles.keytext}>{langs.ordertotal}</Text>
            <Text style={styles.valuetext}>94 {langs.riyal}</Text>
          </View>
          <View style={styles.descwrapper}>
            <Text style={styles.keytext}>{langs.orderdel}</Text>
            <Text style={styles.valuetext}>94 {langs.riyal}</Text>
          </View>
          <View style={styles.descwrapper}>
            <Text style={styles.keytext}>{langs.vat}</Text>
            <Text style={styles.valuetext}>94 {langs.riyal}</Text>
          </View>
          <View style={styles.descwrappertotal}>
            <Text style={[styles.keytext, styles.totalkey]}>
              {langs.finaltotal}
            </Text>
            <Text style={[styles.valuetext, styles.totalvalue]}>
              94 {langs.riyal}
            </Text>
          </View>
        </View>
        <View style={styles.btmcontent}>
          <AppButton
            title={langs.finishbtn}
            onPress={() => {
              navigation.navigate('Checkout');
            }}
          />
        </View>
      </Content>
    </Container>
  );
};

export default CartScreen;
