/*************************************************************/

import {ScaledSheet} from 'react-native-size-matters';
import {ICONS_COLOR, MAIN_COLOR, TEXT_COLOR} from '../../constants/Colors';
import {FONTS} from '../../constants/Fonts';
/*************************************************************/

const {FrutigerLight, Frutigerroman, FrutigerBold, FrutigerBlack} = FONTS;
/*************************************************************/

const styles = ScaledSheet.create({
  conatiner: {
    flex: 1,
    backgroundColor: '#fff',
    // paddingVertical: '100@s',
  },
  prodwrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    paddingVertical: '10@vs',
    paddingHorizontal: '20@s',
    borderBottomColor: '#f0f0f0',
    borderBottomWidth: 1,
  },
  prodswrapper: {
    paddingTop: '5@s',
    // paddingTop: '5@s',
    borderBottomColor: '#f2f2f2',
    borderBottomWidth: 5,
  },
  prodcontainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  price: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  pricetext: {
    fontFamily: Frutigerroman,
    color: MAIN_COLOR,
    fontSize: '15@s',
  },
  text: {
    fontFamily: FrutigerBold,
    fontSize: '15@s',
    color: TEXT_COLOR,
  },
  image: {
    width: '45@s',
    height: '45@s',
    borderRadius: 23,
  },
  imagecontainer: {
    backgroundColor: '#fff',
    width: '60@s',
    height: '60@s',
    borderRadius: 40,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: '10@s',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  titlecontainer: {
    paddingHorizontal: '10@s',
  },
  countwrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row-reverse',
    marginTop: '2@vs',
  },
  numberwrapper: {
    paddingHorizontal: '12@s',
  },
  number: {
    color: TEXT_COLOR,
    fontSize: '13@s',
    fontFamily: Frutigerroman,
  },
  icon: {
    color: ICONS_COLOR,
    fontSize: '10@s',
  },
  iconleft: {
    color: TEXT_COLOR,
    fontSize: '15@s',
    paddingBottom: '10@s',
  },
  input: {
    color: '#a4a4a4',
    fontSize: '12@s',
    fontFamily: Frutigerroman,
    paddingBottom: '10@s',
  },
  coupon: {
    color: TEXT_COLOR,
    fontSize: '15@s',
    fontFamily: FrutigerBold,
    paddingBottom: '20@s',
  },
  inputwrapper: {
    marginTop: '30@s',
    paddingHorizontal: '20@s',
  },
  btmcontent: {
    // flex: 1,
    // justifyContent: 'flex-end',
    // display: 'flex',
    marginBottom: '20@vs',
  },
  descwrapper: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginBottom: '15@s',
  },
  keytext: {fontSize: '15@s', fontFamily: Frutigerroman, color: TEXT_COLOR},
  valuetext: {fontSize: '15@s', fontFamily: Frutigerroman, color: MAIN_COLOR},
  descwrappertotal: {
    borderTopColor: '#f0f0f0',
    borderTopWidth: 1,
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginTop: '20@s',
  },
  totalkey: {
    fontFamily: FrutigerBlack,
  },
  totalvalue: {
    fontFamily: FrutigerBlack,
  },
  btmcontentValues: {
    paddingHorizontal: '20@s',
    marginVertical: '25@s',
  },
  stock: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    flexDirection: 'row-reverse',
    paddingVertical: '12@s',
    backgroundColor: 'rgb(255, 65, 65)',
    paddingHorizontal: '15@s',
  },
  stocktext: {
    color: '#ffffff',
    fontFamily: Frutigerroman,
    fontSize: '12@s',
    paddingHorizontal: '7@s',
  },
  stockicon: {color: '#ffffff', fontSize: '12@s'},
  icondeletewrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    backgroundColor: 'rgb(255, 65, 65)',
  },
  icondelete: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: '#fff',
    fontSize: '20@s',
  },
});
export default styles;
