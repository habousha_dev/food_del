import { Content } from 'native-base';
import React from 'react';
import { Dimensions, Image, Keyboard, Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { AppButton } from '../../components/AppButton';
import { Input } from '../../components/Input';
import { Images } from '../../constants/Images';
import langs from '../../langs/ar';
import styles from './styles';
const win = Dimensions.get('window');
const ratio = win.width / 257; //542 is actual image width

/*************************************************************/

/*************************************************************/

const emailIcon = () => {
  return <Icon name="envelope" solid style={styles.iconrender} />;
};

const ForgetPasswordScreen = ({navigation}) => {
  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <Content style={styles.conatiner}>
        <TouchableOpacity onPress={() => {navigation.goBack()}} style={styles.backbtn}>
          <Icon name="chevron-left" style={styles.backicon} />
        </TouchableOpacity>
        <View style={styles.wrapper}>
          <View style={styles.imagewrapper}>
            <Image
              source={Images.signin_logo}
              style={{...styles.image, width: win.width, height: 100 * ratio}}
              resizeMode="contain"
            />
          </View>
          <View style={styles.signinwrapper}>
            <Text style={styles.signintext}>{langs.forget}</Text>
          </View>
        
          <View style={styles.inputwrraper}>
            <Input
              underlined
              style={styles.input}
              placeholderPosition="right"
              placeholderTextColor="#a4a4a4"
              keyboardType={'email-address'}
              placeholder={langs.enteremail}
              renderIconRight={emailIcon}
              style={styles.inputfix}
              autoFocus={true}
            />
          </View>
          <AppButton title={langs.contbtn} wrapperStyle={styles.appbutton} />
        </View>
      </Content>
    </TouchableWithoutFeedback>
  );
};

export default ForgetPasswordScreen;
