/*************************************************************/

import {ScaledSheet} from 'react-native-size-matters';
import {TEXT_COLOR} from '../../constants/Colors';
import {FONTS} from '../../constants/Fonts';
/*************************************************************/

const {FrutigerLight, Frutigerroman, FrutigerBold, FrutigerBlack} = FONTS;
/*************************************************************/

const styles = ScaledSheet.create({
  conatiner: {
    flex: 1,
    // paddingVertical: '100@s',
    backgroundColor: '#fff',
  },
  wrapper: {
    paddingHorizontal: '25@s',
    // textAlign: 'center',
  },
  backbtn: {
    backgroundColor: '#rgb(216, 216, 216)',
    // width: '45@s',
    paddingVertical: '12@s',
    paddingHorizontal: '18@s',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'flex-end',
    marginTop: '20@vs',
    alignItems: 'center',
    // flexDirection: 'row',
    borderTopStartRadius: '30@s',
    borderBottomLeftRadius: '30@s',
    // position: 'absolute',
    // right: 0,
    // flex: 1,
  },
  backicon: {
    color: '#ffffff',
    fontSize: '18@s',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  // conatiner: {},
  imagewrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: '20@vs',
  },
  socialwrapper: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: '25@vs',
  },
  bywaywrapper: {
    // color: '#rgb(237, 237, 237)',
    borderBottomColor: '#rgb(237, 237, 237)',
    borderBottomWidth: 1,
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: '20@vs',
  },
  forgetwrapper: {
    marginVertical: 30,
  },
  forgettext: {
    fontSize: '13@s',
    fontFamily: Frutigerroman,
    color: TEXT_COLOR,
  },
  rightswrapper: {
    display: 'flex',
    // alignItems: 'flex-end',
    flex: 1,
    justifyContent: 'flex-end',
    marginVertical: '20@s',
  },
  signinwrapper: {},
  signintext: {
    textAlign: 'center',
    color: TEXT_COLOR,
    fontSize: '20@s',
    fontFamily: FrutigerBold,
  },
  image: {},
  facebookwrapper: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    marginHorizontal: '5@s',
    backgroundColor: '#395693',
    paddingHorizontal: '25@s',
    paddingVertical: '10@s',
    borderRadius: 25,
  },
  facebooktext: {
    color: '#ffffff',
    fontFamily: FrutigerBold,
    fontSize: '12@s',
  },
  facebookicon: {
    color: '#ffffff',
    fontSize: '12@s',
    paddingHorizontal: '5@s',
  },
  googlewrapper: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    marginHorizontal: '5@s',
    backgroundColor: '#ffffff',
    marginHorizontal: '5@s',
    // backgroundColor: '#395693',
    paddingHorizontal: '25@s',
    paddingVertical: '10@s',
    borderRadius: 25,
    borderColor: '#rgb(242, 242, 242)',
    borderWidth: 2,
  },
  googletext: {
    fontFamily: FrutigerBold,
    fontSize: '12@s',
    color: '#a4a4a4',
    paddingHorizontal: '5@s',
    // position: 'absolute',
  },
  googleicon: {
    // paddingHorizontal: '10@s',
  },
  fakeline: {
    // color: '#rgb(237, 237, 237)',
    backgroundColor: 'red',
    // height: 10,
  },
  bywaytext: {
    textAlign: 'center',
    fontFamily: FrutigerLight,
    fontSize: '12@s',
    color: '#a4a4a4',
    backgroundColor: '#fff',
    position: 'absolute',
    bottom: -10,
    // right: 50,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: '20@s',
  },
  rights: {
    textAlign: 'center',
    color: '#a4a4a4',
    fontFamily: FrutigerLight,
    fontSize: '11@s',
  },
  iconrender: {
    color: '#a4a4a4',
    fontSize: '15@s',
    paddingBottom: '8@vs',
  },
  inputfix: {
    paddingBottom: '8@vs',
    // color: '#a4a4a4',
    fontSize: '12@s',
    fontFamily: Frutigerroman,
    // marginBottom: 30,
  },
  inputwrraper: {
    marginVertical: '20@s',
  },
  appbutton: {
    marginHorizontal: 0,
    paddingVertical: '10@vs',
  },
  // iconfix: {color: MAIN_COLOR},
});
export default styles;
