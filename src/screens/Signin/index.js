import {Content} from 'native-base';
import React from 'react';
import {
  Dimensions,
  Image,
  Keyboard,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {AppButton} from '../../components/AppButton';
import {Input} from '../../components/Input';
import {Images} from '../../constants/Images';
import langs from '../../langs/ar';
import styles from './styles';
const win = Dimensions.get('window');
const ratio = win.width / 257;
/*************************************************************/

/*************************************************************/
var date = new Date().getFullYear();

const emailIcon = () => {
  return <Icon name="envelope" solid style={styles.iconrender} />;
};
const passIcon = () => {
  return <Icon name="lock" style={styles.iconrender} />;
};

const SignInScreen = ({navigation}) => {
  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <Content style={styles.conatiner}>
        <TouchableOpacity onPress={() => {}} style={styles.backbtn}>
          <Icon name="chevron-left" style={styles.backicon} />
        </TouchableOpacity>
        <View style={styles.wrapper}>
          <View style={styles.imagewrapper}>
            <Image
              source={Images.signin_logo}
              style={{...styles.image, width: win.width, height: 100 * ratio}}
              resizeMode="contain"
            />
          </View>
          <View style={styles.signinwrapper}>
            <Text style={styles.signintext}>{langs.signwith}</Text>
          </View>
          <View style={styles.socialwrapper}>
            <TouchableOpacity style={styles.googlewrapper}>
              <Image source={Images.googleicon} style={styles.googleicon} />
              <Text style={styles.googletext}>{langs.withgoogle}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.facebookwrapper}>
              <Icon name="facebook-f" style={styles.facebookicon} />
              <Text style={styles.facebooktext}>{langs.withfacebook}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.bywaywrapper}>
            <Text style={styles.bywaytext}>{langs.or_by}</Text>
          </View>
          <View style={styles.inputwrraper}>
            <Input
              underlined
              style={styles.input}
              placeholderPosition="right"
              placeholderTextColor="#a4a4a4"
              keyboardType={'email-address'}
              placeholder={langs.email}
              renderIconRight={emailIcon}
              style={styles.inputfix}
              autoFocus={true}
            />
          </View>
          <Input
            underlined
            style={styles.input}
            placeholderPosition="right"
            placeholderTextColor="#a4a4a4"
            placeholder={langs.password}
            secureTextEntry={true}
            renderIconRight={passIcon}
            style={styles.inputfix}
          />
          <TouchableOpacity
            style={styles.forgetwrapper}
            onPress={() => navigation.navigate('Forget')}>
            <Text style={styles.forgettext}>{langs.forget}</Text>
          </TouchableOpacity>
          <AppButton
            title={langs.login}
            wrapperStyle={styles.appbutton}
            onPress={() => {
              navigation.navigate('Home');
            }}
          />
        </View>
        <View style={styles.rightswrapper}>
          <Text style={styles.rights}>
            © {date} {langs.rights}
          </Text>
          <Text style={styles.rights}>{langs.ksa}</Text>
        </View>
      </Content>
    </TouchableWithoutFeedback>
  );
};

export default SignInScreen;
