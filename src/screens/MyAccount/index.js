import {Container, Content} from 'native-base';
import React, {useState} from 'react';
import {Switch, Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {AppButton} from '../../components/AppButton';
import Footerdef from '../../components/footer';
import HeaderCust from '../../components/Headers/Header';
import langs from '../../langs/ar';
import styles from './styles';

/********************************************************* */

const MyAccountScreen = ({navigation}) => {
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);

  const renderItems = () => {
    return (
      <View>
        <View style={styles.borderline}></View>
        <View style={[styles.btmitems, styles.topitems]}>
          <TouchableOpacity
            style={styles.itemwrapper}
            onPress={() => {
              navigation.navigate('AddressBook');
            }}>
            <View style={styles.titlecontainer}>
              <Text style={styles.itemtext}>{langs.addstack}</Text>
              <Icon name="address-book" solid style={styles.itemicon} />
            </View>
            <Icon
              name="chevron-left"
              style={[styles.itemicon, styles.chevicon]}
            />
          </TouchableOpacity>
          <TouchableOpacity style={styles.itemwrapper}>
            <View style={styles.titlecontainer}>
              <Text style={styles.itemtext}>{langs.langchange}</Text>
              <Icon name="globe" style={styles.itemicon} />
            </View>
            <Icon
              name="chevron-left"
              style={[styles.itemicon, styles.chevicon]}
            />
          </TouchableOpacity>
          <TouchableOpacity style={[styles.itemwrapper, styles.nonborder]}>
            <View style={styles.titlecontainer}>
              <Text style={styles.itemtext}>{langs.notify}</Text>
              <Icon name="bell" solid style={styles.itemicon} />
            </View>

            <Switch
              trackColor={{false: '#e5e5e5', true: '#7bbd4a'}}
              thumbColor={isEnabled ? '#7bbd4a' : '#e5e5e5'}
              ios_backgroundColor="#e5e5e5"
              onValueChange={toggleSwitch}
              value={isEnabled}
              // style={{transform: [{scaleX: 0.9}, {scaleY: 0.9}]}}
            />
          </TouchableOpacity>
        </View>

        <View style={styles.borderline}></View>
        <View style={styles.btmitems}>
          <TouchableOpacity
            style={styles.itemwrapper}
            onPress={() => {
              navigation.navigate('Help');
            }}>
            <View style={styles.titlecontainer}>
              <Text style={styles.itemtext}>{langs.help}</Text>
              <Icon name="exclamation-circle" style={styles.itemicon} />
            </View>
            <Icon
              name="chevron-left"
              style={[styles.itemicon, styles.chevicon]}
            />
          </TouchableOpacity>
          <TouchableOpacity style={styles.itemwrapper}>
            <View style={styles.titlecontainer}>
              <Text style={styles.itemtext}>{langs.instruc}</Text>
              <Icon name="gavel" style={styles.itemicon} />
            </View>
            <Icon
              name="chevron-left"
              style={[styles.itemicon, styles.chevicon]}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.itemwrapper}
            onPress={() => {
              navigation.navigate('Privacy');
            }}>
            <View style={styles.titlecontainer}>
              <Text style={styles.itemtext}>{langs.privacy}</Text>
              <Icon name="lock" style={styles.itemicon} />
            </View>
            <Icon
              name="chevron-left"
              style={[styles.itemicon, styles.chevicon]}
            />
          </TouchableOpacity>
          <TouchableOpacity style={styles.itemwrapper}>
            <View style={styles.titlecontainer}>
              <Text style={styles.itemtext}>{langs.rate}</Text>
              <Icon name="star" solid style={styles.itemicon} />
            </View>
            <Icon
              name="chevron-left"
              style={[styles.itemicon, styles.chevicon]}
            />
          </TouchableOpacity>
          <TouchableOpacity style={[styles.itemwrapper, styles.nonborder]}>
            <View style={styles.titlecontainer}>
              <Text style={styles.itemtext}>{langs.sharewith}</Text>
              <Icon name="share-alt" style={styles.itemicon} />
            </View>
            <Icon
              name="chevron-left"
              style={[styles.itemicon, styles.chevicon]}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.borderline}></View>
      </View>
    );
  };
  return (
    <Container>
      <HeaderCust title={`${langs.accheader}`} style={styles.headfix} />
      <Content style={styles.conatiner}>
        {/* TOP HEAD */}
        <View style={styles.topcontainer}>
          <View style={styles.detailswrapper}>
            <Text style={styles.name}>Test User</Text>
            <Text style={styles.email}>Test@User.com</Text>
            <Text style={styles.phone}>+123456488</Text>
          </View>
          <View style={styles.topbtnwrapper}>
            <AppButton
              title={langs.editbtn}
              wrapperStyle={styles.topappbutton}
              titleStyle={styles.topapptext}
              onPress={() => {
                navigation.navigate('EditAccount');
              }}
            />
          </View>
        </View>
        {/* TOP HEAD */}
        {renderItems(navigation)}
        <View style={styles.btnwrapper}>
          <AppButton title={langs.logoutbtn} wrapperStyle={styles.appbutton} />
        </View>
      </Content>
      <Footerdef />
    </Container>
  );
};

export default MyAccountScreen;
