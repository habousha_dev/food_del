/*************************************************************/

import {ScaledSheet} from 'react-native-size-matters';
import {TEXT_COLOR} from '../../constants/Colors';
import {FONTS} from '../../constants/Fonts';
/*************************************************************/

const {FrutigerLight, Frutigerroman, FrutigerBold, FrutigerBlack} = FONTS;
/*************************************************************/

const styles = ScaledSheet.create({
  conatiner: {
    flex: 1,
    paddingVertical: '20@s',
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    elevation: 8,
    // paddingHorizontal: '15@s',
  },
  name: {
    fontFamily: FrutigerBold,
    fontSize: '17@s',
    color: TEXT_COLOR,
  },
  email: {
    fontFamily: FrutigerLight,
    fontSize: '14@s',
    color: '#8e8e8e',
  },
  phone: {
    fontFamily: FrutigerLight,
    fontSize: '14@s',
    color: '#8e8e8e',
  },
  topcontainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: '15@s',
    marginBottom: '15@s',
  },
  topappbutton: {
    marginHorizontal: 0,
    paddingHorizontal: '20@s',
    paddingVertical: '10@s',
  },
  topapptext: {
    fontSize: '14@s',
    fontFamily: FrutigerBold,
  },
  itemwrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomColor: '# rgb(229, 229, 229)',
    borderBottomWidth: 1,
    paddingBottom: '10@s',
    marginBottom: '20@s',
  },
  nonborder: {
    borderBottomWidth: 0,
  },
  btmitems: {
    paddingHorizontal: '15@s',
  },
  borderline: {
    borderTopColor: '#rgb(242, 242, 242)',
    borderTopWidth: 5,
    marginBottom: '20@s',
    paddingHorizontal: 20,

    // paddingHorizontal: -155,
  },
  titlecontainer: {
    display: 'flex',
    flexDirection: 'row-reverse',
    alignItems: 'center',
  },
  itemtext: {
    color: TEXT_COLOR,
    fontSize: '13@s',
    fontFamily: FrutigerBold,
    paddingHorizontal: '8@s',
  },
  itemicon: {
    fontSize: '15@s',
  },
  chevicon: {
    color: '#ababab',
  },
  btnwrapper: {
    // flex: 1,
    justifyContent: 'flex-end',
    marginBottom: '40@s',
  },
  appbutton: {
    backgroundColor: '#ff4141',
    borderColor: '#ff4141',
  },
});
export default styles;
