import {Container, Content} from 'native-base';
import React, {useEffect} from 'react';
import Footerdef from '../../components/footer';
import HeaderCust from '../../components/Headers/Header';
import Order from '../../components/Order';
import langs from '../../langs/ar';
import styles from './styles';

/*************************************************************/

/*************************************************************/
const OrdersScreen = ({navigation, props}) => {
  useEffect(() => {
    return console.log('props', navigation);
  }, []);
  return (
    <Container>
      <HeaderCust title={`${langs.ordersheader}`} />
      <Content style={styles.conatiner}>
        <Order onPress={() => navigation.navigate('OrderStatus')} />
        <Order onPress={() => navigation.navigate('OrderStatus')} />
        <Order onPress={() => navigation.navigate('OrderStatus')} />
        <Order onPress={() => navigation.navigate('OrderStatus')} />
      </Content>
      <Footerdef />
    </Container>
  );
};

export default OrdersScreen;
