/*************************************************************/

import {ScaledSheet} from 'react-native-size-matters';
import {FONTS} from '../../constants/Fonts';
/*************************************************************/

const {FrutigerLight, Frutigerroman, FrutigerBold, FrutigerBlack} = FONTS;
/*************************************************************/

const styles = ScaledSheet.create({
  conatiner: {
    flex: 1,
    backgroundColor: '#fff',
    // paddingVertical: '100@s',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    elevation: 8,
  },
  text: {
    fontFamily: FrutigerBlack,
  },
});
export default styles;
