// const {FrutigerLight, Frutigerroman, FrutigerBold, FrutigerBlack} = FONTS;
import {Content} from 'native-base';
import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import styles from './styles';
// import {FONTS} from './src/constants/Fonts';
// const win = Dimensions.get('window');
// const ratio = win.width / 606; //606 is actual image width
/*************************************************************/
const slides = [
  {
    key: 1,
    title: 'اطلب أكلك بكل سهولة',
    text: 'لدينا تغطية توصيل لجميع أنحاء المملكة',
    image: require('../../../assets/images/walkthrough/walkthrought_one.png'),
    backgroundColor: '#59b2ab',
  },
  {
    key: 2,
    title: 'نوصلك طلبك علي وجه السرعة',
    text: 'لدينا تغطية توصيل لجميع أنحاء المملكة',
    image: require('../../../assets/images/walkthrough/walkthrought_two.png'),
    backgroundColor: '#febe29',
  },
  {
    key: 3,
    title: 'سجل حساب الآن',
    text: 'واستمتع بأشهي الوجبات وأقوي العروض',
    image: require('../../../assets/images/walkthrough/walkthrought_three.png'),
    backgroundColor: '#22bcb5',
  },
];
/*************************************************************/
var currentkey;

class WalkthroughScreen extends Component {
  // constructor(props) {
  //   super(props);
  //   console.log('props', this.props);
  // }
  constructor(props) {
    super(props);
    this.state = {
      showRealApp: false,
      hidePagination: false,
    };
  }
  componentDidMount() {
    console.log('props', this.props);
  }
  // state = {
  //   showRealApp: false,
  //   hidePagination: false,
  // };

  _renderItem = ({item}) => {
    // const {navigation} = this.props;
    return (
      <Content>
        <View style={styles.slide}>
          <View>
            {item.key != 3 ? (
              <TouchableOpacity
                style={styles.donetop}
                onPress={() => {
                  // console.log('regssssssss');
                  // this.props.navigation.navigate('Home');
                  console.log(this.props);
                  // setRealApp(false);
                  // this.setState({
                  //   showRealApp: true,
                  // });
                }}>
                <Text>تخطى</Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity>
                <Text></Text>
              </TouchableOpacity>
            )}
            <Image
              source={item.image}
              style={styles.image}
              resizeMode="contain"
            />
            <View style={styles.textwrapper}>
              <Text style={styles.title}>{item.title}</Text>
              <Text style={styles.text}>{item.text}</Text>
            </View>
          </View>
          {item.key === 3 ? (
            <View style={styles.btnwrapper}>
              <TouchableOpacity
                style={styles.register}
                onPress={() => {
                  console.log('reg');
                  this.props.navigation.navigate('Signup');
                }}>
                <Text style={styles.registertext}>تسجيل حساب جديد</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.login}
                onPress={() => {
                  this.props.navigation.navigate('Signup');
                  console.log('login');
                  console.log(this.props);
                }}>
                <Text style={styles.logintext}>دخول</Text>
              </TouchableOpacity>
            </View>
          ) : null}
        </View>
      </Content>
    );
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <AppIntroSlider
          renderItem={this._renderItem}
          data={slides}
          showNextButton={false}
          showDoneButton={false}
          onSlideChange={(index, lastIndex) => {
            currentkey = index;
            this.setState({
              hidePagination: index == 2,
            });
          }}
          dotStyle={{
            ...styles.nonactive,
            display: currentkey == 2 ? 'none' : 'flex',
          }}
          bottomButton={false}
          hidePagination={this.state.hidePagination}
          activeDotStyle={{
            ...styles.activedot,
            display: currentkey == 2 ? 'none' : 'flex',
          }}
        />
      </View>
    );
  }
}

export default WalkthroughScreen;
