/*************************************************************/

import {Dimensions} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {MAIN_COLOR} from '../../constants/Colors';
import {FONTS} from '../../constants/Fonts';

/*************************************************************/
const win = Dimensions.get('window');
const ratio = win.width / 606; //606 is actual image width
const {FrutigerLight, Frutigerroman, FrutigerBold, FrutigerBlack} = FONTS;
/*************************************************************/

const styles = ScaledSheet.create({
  donetop: {
    backgroundColor: '#rgb(140, 200, 96)',
    display: 'flex',
    alignItems: 'center',
    width: '90@s',
    paddingVertical: '5@vs',
    position: 'absolute',
    top: '20@s',
    left: '15@s',
    borderRadius: 20,
  },
  skitbtn: {
    fontSize: '13@s',
    fontFamily: FrutigerBold,
    color: '#ffffff',
  },
  image: {width: win.width, height: 450 * ratio, marginTop: '120@vs'},
  activedot: {
    backgroundColor: '#ffffff',
    width: '30@s',
    height: '6@s',
  },
  nonactive: {
    backgroundColor: '#9ad96b',
    width: '30@s',
    height: '6@s',
  },
  title: {
    color: '#ffffff',
    fontFamily: FrutigerBold,
    fontSize: '22@s',
  },
  textwrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '20@vs',
  },
  text: {
    color: '#ffffff',
    fontFamily: FrutigerLight,
    fontSize: '18@s',
  },
  slide: {
    position: 'relative',
  },
  register: {
    backgroundColor: '#rgb(255, 255, 255)',
    paddingHorizontal: '25@s',
    paddingVertical: '7@s',
    borderRadius: 50,
  },
  registertext: {
    fontSize: '14@s',
    fontFamily: FrutigerBold,
    color: MAIN_COLOR,
  },
  login: {
    marginTop: '5@vs',
  },
  logintext: {
    color: '#ffffff',
    fontSize: '15@s',
    fontFamily: Frutigerroman,
    borderBottomColor: '#fff',
    borderBottomWidth: 0.5,
  },
  btnwrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: '80@vs',
  },
});
export default styles;
