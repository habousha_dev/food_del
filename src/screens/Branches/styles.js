/*************************************************************/
import {ScaledSheet} from 'react-native-size-matters';
import {MAIN_COLOR} from '../../constants/Colors';
import {FONTS} from '../../constants/Fonts';
/*************************************************************/

const {FrutigerLight, Frutigerroman, FrutigerBold, FrutigerBlack} = FONTS;
/*************************************************************/

const styles = ScaledSheet.create({
  maincontent: {
    // backgroundColor: 'red',
    // flex: 1,
  },
  mapconatiner: {
    // flex: 1,
    position: 'relative',
    marginBottom: '30@vs',
  },
  map: {
    flex: 1,
    height: '510@s',
  },
  iconwrapper: {
    position: 'absolute',
    bottom: '50@vs',
    backgroundColor: MAIN_COLOR,
    padding: 15,
    borderRadius: 25,
    marginHorizontal: 15,
  },
  iconloca: {
    color: '#fff',
    fontSize: '15@s',
  },
  currentwrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: '#fff',
    paddingVertical: '10@s',
    // top: 0,
    left: 0,
    right: 0,
    // bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    // right: Dimensions.get('window').width - 200,
    // bottom: 0,
    // paddingHorizontal: '25@s',
    marginHorizontal: '10@s',
    borderRadius: '25@s',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    position: 'absolute',
    bottom: -20,
  },
  iconcurrent: {
    color: MAIN_COLOR,
    fontSize: '20@s',
  },
  currtext: {
    color: '#5b5b5b',
    fontSize: '13@s',
    fontFamily: Frutigerroman,
  },
  btmcontent: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginVertical: '20@vs',
    flex: 1,
  },
});
export default styles;
