import {Container, Content} from 'native-base';
import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {AppButton} from '../../components/AppButton';
import {Images} from '../../constants/Images';
import langs from '../../langs/ar';
import styles from './styles';

/*************************************************************/

/*************************************************************/

const BranchesScreen = ({props}) => {
  const coordinates = [
    {name: 'Pizza', latitude: 24.726663, longitude: 46.654437},
    {name: 'Salad', latitude: 24.756663, longitude: 46.654437},
    {name: 'Cook', latitude: 24.746667, longitude: 46.694437},
    {name: 'Fish', latitude: 24.706667, longitude: 46.694437},
  ];
  return (
    <Container>
      <Content style={styles.maincontent}>
        <View style={styles.mapconatiner}>
          <MapView
            provider={PROVIDER_GOOGLE} // remove if not using Google Maps
            style={styles.map}
            region={{
              latitude: 24.716663,
              longitude: 46.674438,
              latitudeDelta: 0.15,
              longitudeDelta: 0.15,
            }}>
            <Marker
              coordinate={{latitude: 24.716663, longitude: 46.674438}}
              pinColor={'purple'} // any color
              title={'title'}
              description={'description'}>
              <Image
                source={Images.current_location}
                style={{height: 35, width: 35}}
              />
            </Marker>
            {coordinates.map((marker) => (
              <Marker
                key={marker.name}
                coordinate={{
                  latitude: marker.latitude,
                  longitude: marker.longitude,
                }}
                title={marker.name}>
                <Image
                  source={Images.locate}
                  style={{width: 50, height: 50}}
                  resizeMode="contain"
                />
              </Marker>
            ))}
          </MapView>
          <TouchableOpacity style={styles.iconwrapper}>
            <Icon name="location-arrow" style={styles.iconloca} />
          </TouchableOpacity>
          <View style={styles.currentwrapper}>
            <Text style={styles.currtext}>
              الأخيار، الجزيرة، الرياض 14251 2229، السعودية
            </Text>
          </View>
        </View>
        <View style={styles.btmcontent}>
          <AppButton
            title={langs.contbtn}
            onPress={() => {
              //   navigation.navigate('Home');
            }}
          />
        </View>
      </Content>
    </Container>
  );
};

export default BranchesScreen;
