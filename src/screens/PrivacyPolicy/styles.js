/*************************************************************/

import {ScaledSheet} from 'react-native-size-matters';
import {TEXT_COLOR} from '../../constants/Colors';
import {FONTS} from '../../constants/Fonts';
/*************************************************************/
const {FrutigerLight, Frutigerroman, FrutigerBold, FrutigerBlack} = FONTS;
/*************************************************************/

const styles = ScaledSheet.create({
  conatiner: {
    flex: 1,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    elevation: 8,
  },
  desc: {
    color: TEXT_COLOR,
    fontFamily: FrutigerLight,
    fontSize: '13@s',
    textAlign: 'justify',
  },
  textcontainer: {
    paddingHorizontal: '25@s',
  },
  sub: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    paddingVertical: '5@vs',
  },
  subcontainer: {
    paddingVertical: '30@vs',
  },
  icon: {
    fontSize: '8@s',
    color: TEXT_COLOR,
    paddingLeft: '10@vs',
    paddingTop: '10@vs',
  },
  containerfirst: {
    paddingTop: '25@vs',
  },
});
export default styles;
