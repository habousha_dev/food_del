import {Container, Content} from 'native-base';
import React from 'react';
import {Dimensions, Image, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import HeaderCust from '../../components/Headers/Header';
import {Images} from '../../constants/Images';
import langs from '../../langs/ar';
import styles from './styles';
const win = Dimensions.get('window');
const ratio = win.width / 738; //738 is actual image width

/*************************************************************/
const renderText = () => {
  return (
    <View style={styles.subcontainer}>
      <View style={styles.sub}>
        <Icon name="circle" solid style={styles.icon} />
        <Text style={styles.desc}>
          لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي
        </Text>
      </View>
      <View style={styles.sub}>
        <Icon name="circle" solid style={styles.icon} />
        <Text style={styles.desc}>
          هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم
        </Text>
      </View>
      <View style={styles.sub}>
        <Icon name="circle" solid style={styles.icon} />
        <Text style={styles.desc}>
          هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم
        </Text>
      </View>
      <View style={styles.sub}>
        <Icon name="circle" solid style={styles.icon} />
        <Text style={styles.desc}>
          هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم
        </Text>
      </View>
      <View style={styles.sub}>
        <Icon name="circle" solid style={styles.icon} />
        <Text style={styles.desc}>
          هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم
        </Text>
      </View>
      <View style={styles.sub}>
        <Icon name="circle" solid style={styles.icon} />
        <Text style={styles.desc}>
          هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم
        </Text>
      </View>
      <View style={styles.sub}>
        <Icon name="circle" solid style={styles.icon} />
        <Text style={styles.desc}>
          يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ
        </Text>
      </View>
    </View>
  );
};

const PrivacyScreen = ({props}) => {
  return (
    <Container>
      <HeaderCust title={`${langs.privacyheader}`} />
      <Content style={styles.conatiner}>
        <View>
          <Image
            source={Images.privacy}
            style={{width: win.width, height: 268 * ratio}}
          />
        </View>

        <View style={styles.textcontainer}>
          <View style={styles.containerfirst}>
            <Text style={styles.desc}>
              هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية
              تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى
              النص. إن كنت تريد أن تستخدم نص لوريم إيبسوم ما، عليك أن تتحقق
              أولاً أن ليس هناك أي كلمات أو عبارات محرجة أو غير لائقة مخبأة في
              هذا النص. بينما تعمل جميع مولّدات نصوص لوريم إيبسوم على الإنترنت
              على إعادة تكرار مقاطع من نص لوريم إيبسوم نفسه عدة مرات بما تتطلبه
              الحاجة.
            </Text>
          </View>
          {renderText()}
        </View>
      </Content>
    </Container>
  );
};

export default PrivacyScreen;
