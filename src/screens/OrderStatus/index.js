import {Container, Content} from 'native-base';
import React from 'react';
import {Image, Text, View} from 'react-native';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import {SceneMap, TabBar, TabView} from 'react-native-tab-view';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {AppButton} from '../../components/AppButton';
import HeaderCust from '../../components/Headers/Header';
import {Images} from '../../constants/Images';
import langs from '../../langs/ar';
import styles from './styles';

/*************************************************************/

const orderStatus = ({navigation}) => (
  <View style={styles.headbar}>
    <View style={styles.mainwrapper}>
      <View style={styles.iconwrapper}>
        <Icon name="check" solid style={styles.icon} />
        <View style={styles.fakeline}></View>
      </View>
      <View style={styles.textwrapper}>
        <View>
          <Text style={styles.firsttitle}>{langs.firststep}</Text>
        </View>
        <View style={styles.secwrapper}>
          <Text style={styles.secondtitle}>{langs.firststepover}</Text>
        </View>
      </View>
    </View>
    <View style={styles.mainwrapper}>
      <View style={styles.iconwrapper}>
        <Icon name="check" solid style={styles.icon} />
        <View style={styles.fakeline}></View>
      </View>
      <View style={styles.textwrapper}>
        <View>
          <Text style={styles.firsttitle}>{langs.secondstep}</Text>
        </View>
        <View style={styles.secwrapper}>
          <Text style={styles.secondtitle}>{langs.secondstepover}</Text>
        </View>
      </View>
    </View>
    <View style={styles.mainwrapper}>
      <View style={styles.iconwrapper}>
        <Icon name="check" solid style={styles.icon} />
        <View style={styles.fakeline}></View>
      </View>
      <View style={styles.textwrapper}>
        <View>
          <Text style={styles.firsttitle}>{langs.thirdstep}</Text>
        </View>
        <View style={styles.secwrapper}>
          <Text style={styles.secondtitle}>{langs.thirdstepover}</Text>
        </View>
      </View>
    </View>
    <View style={styles.mainwrapper}>
      <View style={[styles.iconwrapper, styles.iconunfinish]}>
        <Icon name="check" solid style={styles.icon} />
        <View style={{...styles.fakeline, backgroundColor: '#e5e5e5'}}></View>
      </View>
      <View style={styles.textwrapper}>
        <View>
          <Text style={styles.firsttitle}>{langs.fourthstep}</Text>
        </View>
        <View style={styles.secwrapper}>
          <Text style={styles.secondtitle}>{langs.fourthstepover}</Text>
        </View>
      </View>
    </View>
    <View style={styles.mainwrapper}>
      <View style={[styles.iconwrapper, styles.iconunfinish]}>
        <Icon name="check" solid style={styles.icon} />
      </View>
      <View style={styles.textwrapper}>
        <View>
          <Text style={styles.firsttitle}>{langs.fififthstep}</Text>
        </View>
        <View style={styles.secwrapper}>
          <Text style={styles.secondtitle}>{langs.fififstepover}</Text>
        </View>
      </View>
    </View>
  </View>
);
/*************************************************************/
const orderContent = () => {
  return (
    <View style={styles.orderContent}>
      <View style={styles.rightcoloumn}>
        <View style={styles.sizewrapper}>
          <Text style={styles.sizetext}>2 x</Text>
        </View>
        <View style={styles.namewrapper}>
          <View>
            <Text style={styles.name}>وجبة فروج</Text>
          </View>
          <View>
            <Text style={styles.price}> 35 ريال</Text>
          </View>
        </View>
      </View>

      <View style={styles.totalpricewrapper}>
        <Text style={styles.totalprice}>70 ريال</Text>
      </View>
    </View>
  );
};
const orderPayment = () => {
  return (
    <View style={styles.paymentwrapper}>
      <View style={styles.paywrapper}>
        <View style={styles.mainpaywrapper}>
          <View style={styles.paydetail}>
            <View>
              <Text style={styles.paytext}>{langs.ordertotal}</Text>
            </View>
            <View>
              <Text style={styles.payamount}>93 ريال</Text>
            </View>
          </View>
          <View style={styles.paydetail}>
            <View>
              <Text style={styles.paytext}>{langs.orderdel}</Text>
            </View>
            <View>
              <Text style={styles.payamount}>93 ريال</Text>
            </View>
          </View>
          <View style={styles.paydetail}>
            <View>
              <Text style={styles.paytext}>{langs.vat}</Text>
            </View>
            <View>
              <Text style={styles.payamount}>93 ريال</Text>
            </View>
          </View>
        </View>

        <View style={styles.totalwrapper}>
          <View>
            <Text style={styles.totaltext}>{langs.finaltotal}</Text>
          </View>
          <View>
            <Text style={styles.totalamount}>93 ريال</Text>
          </View>
        </View>
      </View>
      {/* how to pay */}
      <View style={styles.howwrapper}>
        <View>
          <Text style={styles.howtext}>{langs.orderhowtopay}</Text>
        </View>
        <View style={styles.howleftwrapper}>
          <Icon name="money-bill-alt" solid style={styles.iconhow} />
          <Text style={styles.texthow}>{langs.money}</Text>
        </View>
      </View>
    </View>
  );
};
const orderMap = () => {
  return (
    <View style={styles.mapcontainerall}>
      <View>
        <Text style={styles.textaddress}>{langs.orderaddress}</Text>
      </View>

      <View style={styles.mapconatiner}>
        <MapView
          provider={PROVIDER_GOOGLE} // remove if not using Google Maps
          style={styles.map}
          region={{
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
          }}>
          <Marker
            coordinate={{latitude: 37.78825, longitude: -122.4324}}
            pinColor={'purple'} // any color
            title={'title'}
            description={'description'}>
            <Image
              source={Images.current_location}
              style={{height: 35, width: 35}}
            />
          </Marker>
        </MapView>
        <View style={styles.currentwrapper}>
          <Text style={styles.currtext}>
            الأخيار، الجزيرة، الرياض 14251 2229، السعودية
          </Text>
        </View>
      </View>
      <View style={styles.btmcontent}>
        <AppButton
          wrapperStyle={styles.appbutton}
          title={langs.returnbtn}
          onPress={() => {
            navigation.navigate('Home');
          }}
        />
      </View>
    </View>
  );
};
/*************************************************************/
const orderDetails = ({props}) => (
  <View style={styles.headbar}>
    <View style={styles.twobtnswrapper}>
      <View style={styles.btndet}>
        <Text style={styles.btntext}>رقم الطلب 2547</Text>
      </View>
      <View style={[styles.btndet, styles.date]}>
        <Text style={{...styles.btntext, color: '#9d9d9d'}}>25 مارس 2019</Text>
      </View>
    </View>
    {orderContent()}
    {orderPayment()}
    {orderMap()}
    {/*  */}
  </View>
);

/*************************************************************/

const OrderStatusScreen = ({props}) => {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'first', title: `${langs.orderstatustab}`},
    {key: 'second', title: `${langs.orderdettab}`},
  ]);

  const renderTabBar = (props) => (
    <TabBar
      {...props}
      indicatorStyle={{backgroundColor: 'transparent'}}
      style={{
        backgroundColor: index === routes ? 'white' : '#7bbd4a',
        borderRadius: 50,
        borderColor: '#7bbd4a',
        borderWidth: 1,
        marginHorizontal: 20,
        fontSize: 26,
      }}
      labelStyle={styles.labelstyleactive}
      inactiveColor="#4f4e4e"
      activeColor="#fff"
    />
  );

  const renderScene = SceneMap({
    first: orderStatus,
    second: orderDetails,
  });

  return (
    <Container>
      <HeaderCust
        title={`  ${langs.orderheader} 2547`}
        style={styles.headfix}
      />
      <Content style={styles.conatiner}>
        <TabView
          navigationState={{index, routes}}
          renderScene={renderScene}
          onIndexChange={setIndex}
          renderTabBar={renderTabBar}
          lazy
          style={
            {
              // paddingHorizontal:50
              // backgroundColor: 'black',
            }
          }
        />
      </Content>
    </Container>
  );
};

export default OrderStatusScreen;
