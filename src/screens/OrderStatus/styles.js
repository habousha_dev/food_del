/*************************************************************/

import {ScaledSheet} from 'react-native-size-matters';
import {MAIN_COLOR, TEXT_COLOR} from '../../constants/Colors';
import {FONTS} from '../../constants/Fonts';
/*************************************************************/

const {FrutigerLight, Frutigerroman, FrutigerBold, FrutigerBlack} = FONTS;
/*************************************************************/

const styles = ScaledSheet.create({
  labelstyleactive: {
    fontFamily: FrutigerBold,
    fontSize: '15@s',
    // color: '#fff',
  },
  labelstyleaInctive: {
    fontFamily: FrutigerBold,
    fontSize: '15@s',
    // color: '#fff',
  },
  // tabstyleactive: {
  //   backgroundColor: '#f1f1f1',
  //   display: 'flex',
  //   flexDirection: 'row',
  //   alignItems: 'center',
  //   justifyContent: 'center',
  //   borderRadius: 50,
  //   borderColor: '#7bbd4a',
  //   borderWidth: 1,
  // },
  conatiner: {
    // flex: 1,
    // paddingHorizontal: '20@s',
    // fontSize: '20@s',
    // backgroundColor: 'green',
    marginTop: '20@s',
    // overflow: 'hidden',
  },

  act: {
    backgroundColor: '#37b372',
    // borderColor: 'white',
    // borderBottomColor: 'red',
    // borderBottomWidth: 10,
    borderRadius: 50,
  },
  fff: {backgroundColor: '#000'},
  tabstyleinactive: {
    backgroundColor: 'black',
  },
  tab: {
    height: 45,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#511A52',
    borderWidth: 5,
    borderRadius: 45 / 2,
  },
  tabFocused: {
    flex: 1,
    height: 60,
    alignItems: 'center',
    padding: 16,
    justifyContent: 'center',
    borderColor: 'green',
    borderWidth: 1,
    borderRadius: 30,
  },
  // orderContent: {
  //   paddingHorizontal: '20@s',
  // },
  // tabstyleactive: {backgroundColor: 'green', borderRadius: 50},
  iconwrapper: {
    width: '25@s',
    height: '25@s',
    backgroundColor: '#rgb(123, 189, 74)',
    borderRadius: 25,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  fakeline: {
    position: 'absolute',
    width: 5,
    height: 70,
    backgroundColor: '#rgb(123, 189, 74)',
    top: 25,
  },
  iconunfinish: {
    backgroundColor: '#e5e5e5',
  },
  icon: {
    fontSize: '12@s',
    color: '#fff',
  },
  mainwrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    paddingHorizontal: '20@s',

    // marginBottom: '22@s',
    // justifyContent: 'space-between',
  },
  textwrapper: {
    marginHorizontal: '15@s',
    paddingTop: '22@s',
  },
  secwrapper: {
    marginTop: '5@vs',
  },
  firsttitle: {
    fontSize: '14@s',
    color: TEXT_COLOR,
    fontFamily: FrutigerBold,
  },
  secondtitle: {
    color: '#8e8e8e',
    fontSize: '15@s',
    fontFamily: FrutigerLight,
  },
  /**order Details styles */
  twobtnswrapper: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    marginVertical: '20@vs',
    paddingHorizontal: '20@s',
  },
  btndet: {
    backgroundColor: '#f0c632',
    paddingHorizontal: '10@s',
    paddingVertical: '3@s',
    borderRadius: 20,
  },
  btntext: {color: '#fff', fontSize: '10@s', fontFamily: FrutigerBold},
  date: {marginHorizontal: '5@s', backgroundColor: '#f2f2f2'},
  rightcoloumn: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  orderContent: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderBottomColor: '#rgb(229, 229, 229)',
    borderBottomWidth: 1,
    paddingBottom: '10@s',
    marginBottom: '10@vs',
    paddingHorizontal: '20@s',
  },
  paymentwrapper: {
    paddingHorizontal: '20@s',
  },
  namewrapper: {
    marginHorizontal: '10@s',
  },
  sizetext: {
    color: TEXT_COLOR,
    fontSize: '12@s',
    fontFamily: FrutigerBold,
  },
  name: {
    color: TEXT_COLOR,
    fontSize: '14@s',
    fontFamily: FrutigerBold,
  },
  price: {
    color: '#8e8e8e',
    fontSize: '12@s',
    fontFamily: FrutigerLight,
  },
  totalprice: {
    color: MAIN_COLOR,
    fontSize: '14@s',
    fontFamily: FrutigerBold,
  },
  mainpaywrapper: {
    display: 'flex',
    borderBottomColor: '#rgb(229, 229, 229)',
    borderBottomWidth: 1,
    paddingBottom: '10@s',
    // alignItems: 'center',
    // justifyContent: 'center',
    // flexDirection: 'row',
  },
  paydetail: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginTop: '5@s',
  },
  paytext: {
    color: TEXT_COLOR,
    fontSize: '15@s',
    fontFamily: Frutigerroman,
  },
  payamount: {
    color: MAIN_COLOR,
    fontSize: '15@s',
    fontFamily: Frutigerroman,
  },
  totaltext: {
    color: TEXT_COLOR,
    fontSize: '17@s',
    fontFamily: FrutigerBlack,
  },
  totalamount: {
    color: MAIN_COLOR,
    fontSize: '16@s',
    fontFamily: FrutigerBlack,
  },
  totalwrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginTop: '5@s',
  },
  howwrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderTopColor: '#rgb(242, 242, 242)',
    borderTopWidth: 5,
    borderBottomColor: '#rgb(242, 242, 242)',
    borderBottomWidth: 5,
    paddingVertical: '10@vs',
    marginVertical: '20@vs',
  },
  howtext: {
    color: TEXT_COLOR,
    fontSize: '15@s',
    fontFamily: Frutigerroman,
  },
  howleftwrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
    // paddingHorizontal: '25@s',
    marginHorizontal: '3@s',
    width: '80@s',
    height: '50@s',
    borderRadius: 10,
    // paddingVertical: '10@s',
  },
  texthow: {
    color: MAIN_COLOR,
    fontSize: '13@s',
    fontFamily: FrutigerBold,
  },
  iconhow: {
    color: MAIN_COLOR,
    fontSize: '15@s',
  },
  /**map */

  map: {
    flex: 1,
  },
  currentwrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: '#fff',
    paddingVertical: '10@s',
    // top: 0,
    left: 0,
    right: 0,
    // bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    // right: Dimensions.get('window').width - 200,
    // bottom: 0,
    // paddingHorizontal: '25@s',
    marginHorizontal: '10@s',
    borderRadius: '25@s',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    position: 'absolute',
    bottom: -20,
  },
  currtext: {
    color: '#5b5b5b',
    fontSize: '12@s',
    fontFamily: Frutigerroman,
  },
  mapconatiner: {
    height: '200@s',
    position: 'relative',
  },

  textaddress: {
    color: TEXT_COLOR,
    fontSize: '14@s',
    fontFamily: Frutigerroman,
    paddingBottom: '8@vs',
    paddingHorizontal: '20@s',
  },
  appbutton: {
    marginHorizontal: 0,
  },
  btmcontent: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginVertical: '20@vs',
    marginTop: '50@vs',
    paddingHorizontal: '20@s',
  },
});
export default styles;
