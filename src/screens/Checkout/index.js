import {Container} from 'native-base';
import React, {useState} from 'react';
import {
  Image,
  Modal,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {AppButton} from '../../components/AppButton';
import HeaderCust from '../../components/Headers/Header';
import {Images} from '../../constants/Images';
import langs from '../../langs/ar';
import styles from './styles';

/*************************************************************/

const CheckoutScreen = ({navigation, props}) => {
  const [date, setDate] = useState('2020-05-15');
  const [modalOpen, setmodalOpen] = useState(false);

  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    console.log('date', date);
    hideDatePicker();
  };
  /*************************************************************/
  const modalContent = () => {
    return (
      <View style={styles.modalcontentwrapper}>
        <TouchableOpacity onPress={() => {}} style={styles.addwrapper}>
          <Text style={styles.addname}>Test user</Text>
          <View style={styles.addcontentwrapper}>
            <Text style={styles.addtext}>
              الأخيار، الجزيرة، الرياض 14251 2229، السعودية
            </Text>
          </View>
          <Text style={styles.phone}>01000000000</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.modalicon}>
          <Icon name="home" style={styles.iconcheck} />
        </TouchableOpacity>
      </View>
    );
  };
  /*************************************************************/
  const modalAddress = () => {
    return (
      <Modal
        visible={modalOpen}
        animationType="fade"
        transparent={true}
        onRequestClose={() => {
          alert('Modal has been closed.');
        }}>
        <View style={styles.modalwrapper}>
          <TouchableOpacity
            onPress={() => setmodalOpen(false)}
            style={styles.iconmodalwrapper}>
            <Image source={Images.close_icon} style={{width: 30, height: 30}} />
          </TouchableOpacity>
          <ScrollView style={styles.innermodalwrapper}>
            {modalContent()}
            {modalContent()}
            {modalContent()}
            {modalContent()}
            {modalContent()}
            {modalContent()}
            {modalContent()}
            {modalContent()}
            <View style={styles.btmcontentModal}>
              <AppButton
                wrapperStyle={styles.appbutton}
                title={langs.contbtn}
                onPress={() => {
                  // navigation.navigate('Home');
                }}
              />
            </View>
          </ScrollView>
        </View>
      </Modal>
    );
  };
  /*************************************************************/

  return (
    <Container>
      <HeaderCust title={`${langs.checkheader}`} />
      <ScrollView style={styles.conatinerss}>
        {modalAddress()}
        {/* {modalContent()} */}
        <View style={styles.conatiner}>
          <MapView
            provider={PROVIDER_GOOGLE} // remove if not using Google Maps
            style={styles.map}
            region={{
              latitude: 37.78825,
              longitude: -122.4324,
              latitudeDelta: 0.015,
              longitudeDelta: 0.0121,
            }}>
            <Marker
              coordinate={{latitude: 37.78825, longitude: -122.4324}}
              pinColor={'purple'} // any color
              title={'title'}
              description={'description'}>
              <Image
                source={Images.current_location}
                style={{height: 35, width: 35}}
              />
            </Marker>
          </MapView>
          <TouchableOpacity style={styles.iconwrapper}>
            <Icon name="location-arrow" style={styles.iconloca} />
          </TouchableOpacity>
          <View style={styles.currentwrapper}>
            <Text style={styles.currtext}>
              الأخيار، الجزيرة، الرياض 14251 2229، السعودية
            </Text>

            <TouchableOpacity
              onPress={() => {
                setmodalOpen(true);
              }}>
              <Icon name="address-book" solid style={styles.iconcurrent} />
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.wayswrapper}>
          <View style={styles.maincontaner}>
            <Text style={styles.howtext}>{langs.paytype}</Text>
            <View style={styles.paywrapper}>
              <TouchableOpacity style={styles.howwrapper} onPress={() => {}}>
                <Icon style={styles.payicon} solid name="money-bill-alt" />
                <Text style={styles.paytext}>{langs.money}</Text>
              </TouchableOpacity>
              <DateTimePickerModal
                isVisible={isDatePickerVisible}
                mode="time"
                onConfirm={handleConfirm}
                onCancel={hideDatePicker}
              />
              <TouchableOpacity
                style={[styles.howwrapper, styles.selctedhow]}
                onPress={() => {}}>
                <Icon
                  style={[styles.payicon, styles.selcteicon]}
                  name="credit-card"
                />
                <Text style={[styles.paytext, styles.selctedtext]}>
                  {langs.visa}
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.maincontaner}>
            <Text style={styles.howtext}>{langs.timeorder}</Text>
            <View style={styles.paywrapper}>
              <TouchableOpacity style={styles.howwrapper} onPress={() => {}}>
                <Icon style={styles.payicon} solid name="stopwatch" />
                <Text style={styles.paytext}>{langs.now}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.howwrapper, styles.selctedhow]}
                onPress={showDatePicker}>
                <Icon
                  style={[styles.payicon, styles.selcteicon]}
                  name="clock"
                />
                <Text style={[styles.paytext, styles.selctedtext]}>
                  {langs.custdate}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <View style={styles.btmcontent}>
          <AppButton
            title={langs.finishbtn}
            onPress={() => {
              navigation.navigate('Status');
            }}
          />
        </View>
      </ScrollView>
    </Container>
  );
};

export default CheckoutScreen;
