/*************************************************************/

import {Dimensions} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {MAIN_COLOR, TEXT_COLOR} from '../../constants/Colors';
import {FONTS} from '../../constants/Fonts';
/*************************************************************/

const {FrutigerLight, Frutigerroman, FrutigerBold, FrutigerBlack} = FONTS;
/*************************************************************/

const styles = ScaledSheet.create({
  conatinerss: {
    flex: 1,
    height: '100%', // But these wouldn't hurt.
  },
  conatiner: {
    height: '200@s',
    position: 'relative',
  },
  map: {
    flex: 1,
  },
  iconwrapper: {
    position: 'absolute',
    bottom: '50@vs',
    backgroundColor: MAIN_COLOR,
    padding: 15,
    borderRadius: 25,
    marginHorizontal: 15,
  },
  iconloca: {
    color: '#fff',
    fontSize: '15@s',
  },
  currentwrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: '#fff',
    paddingVertical: '10@s',
    paddingHorizontal: '20@s',
    marginHorizontal: '10@s',
    borderRadius: '25@s',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  iconcurrent: {
    color: MAIN_COLOR,
    fontSize: '20@s',
  },
  currtext: {
    color: '#5b5b5b',
    fontSize: '13@s',
    fontFamily: Frutigerroman,
  },
  btmcontent: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginVertical: '20@vs',
  },
  //
  paywrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  howwrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    backgroundColor: MAIN_COLOR,
    marginHorizontal: '10@s',
    paddingVertical: '15@vs',
    width: '140@s',
    borderRadius: 10,
  },
  maincontaner: {
    marginTop: '30@vs',
  },
  wayswrapper: {
    marginTop: '5@s',
  },
  howtext: {
    color: '#4f4e4e',
    fontSize: '20@s',
    textAlign: 'center',
    marginBottom: '10@vs',
    fontFamily: FrutigerBold,
  },
  paytext: {
    color: '#ffffff',
    fontSize: '15@s',
    fontFamily: FrutigerBold,
  },
  payicon: {
    color: '#ffffff',
    fontSize: '25@s',
    marginBottom: '5@vs',
  },
  selctedhow: {
    backgroundColor: '#ffffff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  selcteicon: {
    color: MAIN_COLOR,
  },
  selctedtext: {
    color: MAIN_COLOR,
  },
  // modal style
  modalwrapper: {
    backgroundColor: '#000000aa',
    flex: 1,
    zIndex: 1,
    paddingTop: 20,
    position: 'relative',
  },
  innermodalwrapper: {
    marginTop: '60@s',
    marginHorizontal: '10@s',
    paddingVertical: 20,
    backgroundColor: '#fff',
    position: 'relative',
    maxHeight: '400@vs',
    borderRadius: 20,
    paddingHorizontal: '20@s',
    zIndex: 1,
  },
  iconmodalwrapper: {
    position: 'absolute',
    top: 30,
    left: Dimensions.get('window').width / 2 - 25,
    bottom: 0,
    backgroundColor: '#a8a8a8',
    height: '40@s',
    width: '40@s',
    borderRadius: 25,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 1,
  },
  btmcontentModal: {
    flex: 1,
    justifyContent: 'flex-end',
    display: 'flex',
    marginVertical: '20@vs',
    marginBottom: 30,
  },
  modalcontentwrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderBottomColor: '#e5e5e5',
    borderBottomWidth: 1,
    paddingBottom: 5,
  },
  addwrapper: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  addname: {
    color: '#a4a4a4',
    fontSize: '15@s',
    fontFamily: Frutigerroman,
  },
  addcontentwrapper: {
    marginVertical: '3@vs',
  },
  addtext: {
    color: TEXT_COLOR,
    fontSize: '13@s',
    fontFamily: FrutigerLight,
  },
  phone: {
    color: TEXT_COLOR,
    fontSize: '13@s',
    fontFamily: FrutigerBold,
  },
  iconcheck: {
    color: MAIN_COLOR,
    fontSize: '20@s',
  },
  appbutton: {
    marginHorizontal: 0,
  },
});
export default styles;
