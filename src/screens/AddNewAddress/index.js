import {Container, Content} from 'native-base';
import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {AppButton} from '../../components/AppButton';
import HeaderCust from '../../components/Headers/Header';
import {Input} from '../../components/Input';
import langs from '../../langs/ar';
import styles from './styles';

/*************************************************************/

/*************************************************************/

const AddNewAddressScreen = ({navigation}) => {
  return (
    <Container>
      <HeaderCust title={` ${langs.addnewaddress}`} style={styles.headfix} />
      <Content style={styles.conatiner}>
        <View style={styles.inputwrraper}>
          <View style={styles.textwrapper}>
            <Text style={styles.text}>{langs.fullname}</Text>
          </View>
          <Input
            placeholder="TEST"
            underlined
            style={styles.input}
            placeholderPosition="right"
            placeholderTextColor="#4f4e4e"
          />
        </View>

        <View style={styles.inputwrraper}>
          <View style={styles.textwrapper}>
            <Text style={styles.text}>{langs.country}</Text>
          </View>
          <RNPickerSelect
            onValueChange={(value) => console.log(value)}
            items={[
              {label: 'السعودية', value: 'السعودية'},
              {label: 'السعودية', value: 'السعودية'},
              {label: 'السعودية', value: 'السعودية'},
            ]}
            placeholder={{}}
            useNativeAndroidPickerStyle={false}
            Icon={() => {
              return <Icon name="chevron-down" color="#bababa" />;
            }}
            style={{
              ...pickerSelectStyles,
              iconContainer: {
                top: 20,
              },
            }}
          />
        </View>
        <View style={styles.inputwrraper}>
          <View style={styles.textwrapper}>
            <Text style={styles.text}>{langs.zone}</Text>
          </View>
          <RNPickerSelect
            onValueChange={(value) => console.log(value)}
            items={[
              {label: 'السعودية', value: 'السعودية'},
              {label: 'السعودية', value: 'السعودية'},
              {label: 'السعودية', value: 'السعودية'},
            ]}
            placeholder={{}}
            useNativeAndroidPickerStyle={false}
            Icon={() => {
              return <Icon name="chevron-down" color="#bababa" />;
            }}
            style={{
              ...pickerSelectStyles,
              iconContainer: {
                top: 20,
              },
            }}
          />
        </View>
        <View style={styles.inputwrraper}>
          <View style={styles.textwrapper}>
            <Text style={styles.text}>{langs.city}</Text>
          </View>
          <RNPickerSelect
            onValueChange={(value) => console.log(value)}
            items={[
              {label: 'السعودية', value: 'السعودية'},
              {label: 'السعودية', value: 'السعودية'},
              {label: 'السعودية', value: 'السعودية'},
            ]}
            placeholder={{}}
            useNativeAndroidPickerStyle={false}
            Icon={() => {
              return <Icon name="chevron-down" color="#bababa" />;
            }}
            style={{
              ...pickerSelectStyles,
              iconContainer: {
                top: 20,
              },
            }}
          />
        </View>

        <View style={styles.inputwrraper}>
          <View style={styles.textwrapper}>
            <Text style={styles.text}>{langs.route}</Text>
          </View>
          <Input
            placeholder="TEST"
            underlined
            style={styles.input}
            placeholderPosition="right"
            placeholderTextColor="#4f4e4e"
          />
        </View>
        <View style={styles.inputwrraper}>
          <View style={styles.textwrapper}>
            <Text style={styles.text}>{langs.bulidnum}</Text>
          </View>
          <Input
            placeholder="TEST"
            underlined
            style={styles.input}
            placeholderPosition="right"
            placeholderTextColor="#4f4e4e"
            keyboardType="number-pad"
          />
        </View>
        <View style={styles.inputwrraper}>
          <View style={styles.textwrapper}>
            <Text style={styles.text}>{langs.flatnum}</Text>
          </View>
          <Input
            placeholder="TEST"
            underlined
            style={styles.input}
            placeholderPosition="right"
            placeholderTextColor="#4f4e4e"
            keyboardType="number-pad"
          />
        </View>
        <View style={styles.inputwrraper}>
          <View style={styles.textwrapper}>
            <Text style={styles.text}>{langs.phone}</Text>
          </View>
          <Input
            placeholder="TEST"
            underlined
            style={styles.input}
            placeholderPosition="right"
            placeholderTextColor="#4f4e4e"
            keyboardType="phone-pad"
          />
        </View>
        <View style={styles.btmcontentModal}>
          <AppButton
            wrapperStyle={styles.appbutton}
            title={langs.savebtn}
            onPress={() => {
              navigation.navigate('Home');
            }}
          />
        </View>
      </Content>
    </Container>
  );
};
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    color: '#4f4e4e',
    // paddingRight: 20, // to ensure the text is never behind the icon
    fontFamily: 'fbold',
    borderBottomColor: 'rgb(229, 229, 229)',
    borderBottomWidth: 1,
  },
  inputAndroid: {
    fontSize: 16,
    color: '#4f4e4e',
    // paddingRight: 20, // to ensure the text is never behind the icon
    fontFamily: 'fbold',
    borderBottomColor: 'rgb(229, 229, 229)',
    borderBottomWidth: 1,
  },
  Icon: {},
});

export default AddNewAddressScreen;
