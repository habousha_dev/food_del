/*************************************************************/

import {ScaledSheet} from 'react-native-size-matters';
import {FONTS} from '../../constants/Fonts';
/*************************************************************/

const {FrutigerLight, Frutigerroman, FrutigerBold, FrutigerBlack} = FONTS;
/*************************************************************/

const styles = ScaledSheet.create({
  conatiner: {
    flex: 1,
    paddingVertical: '20@s',
    paddingHorizontal: '20@s',
  },
  text: {
    fontFamily: Frutigerroman,
    fontSize: '13@s',
    color: '#a4a4a4',
  },
  inputwrraper: {
    marginBottom: '17@s',
  },
  btnwrapper: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginBottom: '40@vs',
  },

  input: {
    paddingBottom: '5@vs',
    fontFamily: FrutigerBold,
  },
  btmcontentModal: {
    flex: 1,
    justifyContent: 'flex-end',
    display: 'flex',
    marginVertical: '20@vs',
    marginBottom: 50,
    marginHorizontal: 0,
    paddingHorizontal: 0,
  },
  appbutton: {
    marginHorizontal: 0,
  },
  //
  // textpicker: {
  //   color: '#333',
  //   // fontSize: RFValue(20, 812),
  //   fontFamily: 'f_bold',
  //   marginLeft: 20,
  // },
  // inputpicker: {
  //   justifyContent: 'flex-start',
  //   height: 60,
  //   borderColor: 'rgb(245, 245, 245)',
  //   borderWidth: 2,
  //   borderRadius: 5,
  //   marginTop: 20,
  //   marginBottom: 15,
  //   position: 'relative',
  // },
  // inputTxt: {
  //   color: '#4f4f4f',
  //   // fontSize: RFValue(18, 812),
  //   backgroundColor: '#fff',
  //   paddingLeft: 20,
  //   borderColor: '#fafafa',
  //   borderWidth: 2,
  // },
});

export default styles;
