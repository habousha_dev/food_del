/*************************************************************/

import {Dimensions} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {MAIN_COLOR, TEXT_COLOR} from '../../constants/Colors';
import {FONTS} from '../../constants/Fonts';
/*************************************************************/

const {FrutigerLight, Frutigerroman, FrutigerBold, FrutigerBlack} = FONTS;
/*************************************************************/

const styles = ScaledSheet.create({
  conatiner: {
    flex: 1,
    backgroundColor: '#fff',
  },
  tophead: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: '15@s',
    paddingTop: '20@vs',
  },
  icons: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  icon: {
    color: TEXT_COLOR,
    fontSize: '25@s',
  },
  iconwrapper: {
    position: 'relative',
  },
  count: {
    fontFamily: FrutigerBold,
    fontSize: '10@s',
    color: '#fff',
  },
  countwrapper: {
    position: 'absolute',
    top: -10,
    left: -10,
    height: '15@vs',
    width: '15@vs',
    backgroundColor: '#f0c632',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 25,
  },
  iconoverride: {
    marginHorizontal: '20@s',
  },
  bagcolor: {
    backgroundColor: '#7bbd4a',
  },
  inputfix: {
    fontFamily: FrutigerBold,
    paddingHorizontal: '15@s',
  },
  inputwrapper: {
    paddingHorizontal: '15@s',
    marginTop: '20@vs',
  },
  wrapperinput: {
    borderWidth: 2,
    borderRadius: '20@s',
    borderColor: '#ebebeb',
    paddingVertical: '7@vs',
  },
  seacrchicon: {
    color: TEXT_COLOR,
    fontSize: '15@s',
    paddingHorizontal: '10@s',
  },
  categwrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: '#f2f2f2',
    marginHorizontal: '5@s',
    borderRadius: 50,
    marginTop: '35@vs',
    marginBottom: '20@vs',
  },
  inputmoda: {
    paddingVertical: '10@vs',
    marginBottom: '10@vs',
    borderRadius: 60,
  },
  categtitle: {
    color: '#969696',
    fontSize: '13@s',
    fontFamily: FrutigerBold,
    paddingHorizontal: '20@s',
    paddingVertical: '7@vs',
  },
  prodwrapper: {
    flex: 1,
  },
  mealwrappercont: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    elevation: 5,
    borderColor: 'red',
    borderWidth: 1,
  },
  imagefix: {
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4.65,
    elevation: 8,
    borderRadius: '100@s',
    overflow: 'hidden',
    padding: '5@s',
  },
  textfix: {
    marginTop: '10@s',
  },
  mealtitle: {
    color: TEXT_COLOR,
    fontSize: '12@s',
    fontFamily: FrutigerBold,
  },

  mealwrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
    marginBottom: '20@vs',
    paddingHorizontal: '3@s',
  },
  //
  // modal style
  modalwrapper: {
    backgroundColor: '#000000aa',
    flex: 1,
    zIndex: 1,
    paddingTop: 20,
    position: 'relative',
  },
  innermodalwrapper: {
    marginTop: '60@s',
    marginHorizontal: '10@s',
    // paddingVertical: 20,
    backgroundColor: '#fff',
    position: 'relative',
    maxHeight: '450@vs',
    borderRadius: 20,
    paddingHorizontal: '20@s',
    zIndex: 1,
  },
  wrapperfix: {
    paddingHorizontal: '20@s',
  },
  iconmodalwrapper: {
    position: 'absolute',
    top: 30,
    left: Dimensions.get('window').width / 2 - 25,
    bottom: 0,
    backgroundColor: '#a8a8a8',
    height: '40@s',
    width: '40@s',
    // paddingVertical: '25@s',
    // paddingHorizontal: '25@s',

    borderRadius: 25,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 1,
  },
  btmcontentModal: {
    flex: 1,
    justifyContent: 'flex-end',
    display: 'flex',
    marginVertical: '20@vs',
    marginBottom: 30,
    paddingHorizontal: '15@s',
  },
  modalcontentwrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    // borderBottomColor: '#e5e5e5',
    // borderBottomWidth: 1,
    paddingBottom: 5,
  },
  addwrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    position: 'relative',
    flex: 1,
  },
  wishwrapper: {
    position: 'absolute',
    top: '20@vs',
    display: 'flex',
    marginHorizontal: '10@s',
    alignSelf: 'flex-start',
  },
  modalcontentwrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headtext: {
    color: TEXT_COLOR,
    fontSize: '18@s',
    fontFamily: FrutigerBold,
  },
  addtext: {
    color: TEXT_COLOR,
    fontSize: '13@s',
    fontFamily: FrutigerBold,
    textAlign: 'center',
  },
  inputtext: {
    color: TEXT_COLOR,
    fontSize: '15@s',
    fontFamily: FrutigerBold,
    paddingBottom: '5@s',
  },
  inputwrppermodal: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  inputmodal: {
    borderBottomColor: '#dbdbdb',
    borderBottomWidth: 1,
    // width: '100%',
    color: '#a4a4a4',
    fontSize: '14@s',
    fontFamily: FrutigerLight,
    width: Dimensions.get('window').width - 100,
    // paddingHorizontal: 100,
  },
  starswrapper: {
    marginVertical: '20@vs',
  },
  appbutton: {
    marginHorizontal: 0,
  },
  buttonStyle: {
    backgroundColor: 'red',
  },
  // PRODUCT MODAL
  productwrapper: {
    maxHeight: '100%',
    // overflow: 'hidden',
    marginBottom: 20,
    paddingHorizontal: 0,
  },
  swipermain: {
    backgroundColor: '#rgba(0, 0, 0,.031)',
    flex: 1,
  },
  prodname: {
    fontSize: '16@s',
    color: TEXT_COLOR,
    fontFamily: FrutigerBold,
  },
  prodprice: {
    fontSize: '16@s',
    color: MAIN_COLOR,
    fontFamily: FrutigerBlack,
  },
  boxone: {
    backgroundColor: '# rgb(242, 242, 242)',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginHorizontal: '5@s',
    paddingHorizontal: '12@s',
    paddingVertical: '3@vs',
    borderRadius: 20,
  },
  boxeswrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginBottom: '15@vs',
  },
  boxtext: {
    color: '#969696',
    fontSize: '10@s',
    fontFamily: Frutigerroman,
  },
  boxicon: {
    color: '#fff',
    paddingHorizontal: '7@s',
  },
  prodnamewrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: '15@vs',
  },
  prodescription: {
    color: '#8b8a8a',
    fontSize: '12@s',
    fontFamily: FrutigerLight,
    textAlign: 'center',
  },
  sideswrapper: {
    borderTopColor: '#rgb(242, 242, 242)',
    borderTopWidth: 5,
    paddingTop: '15@s',
    marginTop: '15@s',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    // borderTopWidth: 5,
    // width: '100%',
  },
  sidetext: {
    color: TEXT_COLOR,
    fontSize: '12@s',
    fontFamily: FrutigerBold,
  },
  nameside: {
    fontSize: '13@s',
    fontFamily: FrutigerLight,
    color: TEXT_COLOR,
    paddingHorizontal: '10@s',
  },
  priceside: {
    fontSize: '13@s',
    fontFamily: FrutigerLight,
    color: MAIN_COLOR,
  },
  sideitemwrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderBottomColor: '#rgb(233, 233, 233)',
    borderBottomWidth: 1,
    paddingBottom: '12@s',
    marginTop: '15@s',
    // flex: 1,
  },
  rightside: {
    display: 'flex',
    alignItems: 'center',
    // justifyContent: 'space-between',
    flexDirection: 'row',
  },
  //swiper
  swiperwrapper: {
    height: '250@s',
    backgroundColor: 'rgba(0, 0, 0, 0.031)',
    position: 'relative',
    paddingTop: '30@s',

    // flex: 1,
  },
  hearticon: {
    fontSize: '20@s',
    // backgroundColor: '#fff',
  },
  slide1: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  imagecenter: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  dotslider: {
    width: 9,
    height: 9,
    borderRadius: 4.5,
    marginLeft: 3,
    marginRight: 3,
    marginTop: 3,
    marginBottom: 3,
  },
});
export default styles;
