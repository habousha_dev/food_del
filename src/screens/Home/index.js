import {Container, Content} from 'native-base';
import React, {useState} from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  Modal,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import StarRating from 'react-native-star-rating';
import Swiper from 'react-native-swiper';
/*************************************************************/
import Icon from 'react-native-vector-icons/FontAwesome5';
import {AppButton} from '../../components/AppButton';
import Footerdef from '../../components/footer';
import {Input} from '../../components/Input';
import {Images} from '../../constants/Images';
import langs from '../../langs/ar';
import styles from './styles';

/*************************************************************/
const win = Dimensions.get('window');
const ratio = win.width / 500; //500 is actual image width
const clerkimg = win.width / 160; //160 is actual image width
const popup_image = win.width / 212; //130 is actual image width

// const img_ratio = 175 / 150;
// const static_width = Dimensions.get('window').width / 3;

/*************************************************************/
const CATEGORIES = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'جميع المنتجات',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'اdddلخاصة',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'اaaaaلخاصة',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'الخاصة',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'الخاصة',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'الخاصة',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'الخاصة',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'الخاصة',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'الخاصة',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'الخاصة',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'رشاقة',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'رشاقة',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'رشاقة',
  },
];
const MEALS = [
  {
    image: require('../../../assets/images/product1.png'),
    title: 'جميع المنتجات',
  },
  {
    image: require('../../../assets/images/offer_first.png'),
    title: 'اdddلخاصة',
  },
  {
    image: require('../../../assets/images/product1.png'),
    title: 'اaaaaلخاصة',
  },
  {
    image: require('../../../assets/images/product2.png'),
    title: 'الخاصة',
  },
  {
    image: require('../../../assets/images/product2.png'),
    title: 'الخاصة',
  },
  {
    image: require('../../../assets/images/product2.png'),
    title: 'الخاصة',
  },
  {
    image: require('../../../assets/images/product3.png'),
    title: 'الخاصة',
  },
  {
    image: require('../../../assets/images/product3.png'),
    title: 'الخاصة',
  },
  {
    image: require('../../../assets/images/product3.png'),
    title: 'الخاصة',
  },
  {
    image: require('../../../assets/images/product2.png'),
    title: 'الخاصة',
  },
  {
    image: require('../../../assets/images/product2.png'),
    title: 'رشاقة',
  },
  {
    image: require('../../../assets/images/product1.png'),
    title: 'رشاقة',
  },
  {
    image: require('../../../assets/images/product1.png'),
    title: 'رشاقة',
  },
];

/*************************************************************/

const HomeScreen = ({navigation}) => {
  const [modalOpen, setmodalOpen] = useState(true);
  const [modalproduct, setmodalproduct] = useState(false);
  const [starCount, setStarCount] = useState(3);

  const onStarRatingPress = (rating) => {
    setStarCount(rating);
  };
  /*************************************************************/

  const renderItem = ({item}) => {
    return (
      <TouchableOpacity style={styles.categwrapper} onPress={() => {}}>
        <Text style={styles.categtitle}>{item.title}</Text>
      </TouchableOpacity>
    );
  };
  const renderMeal = ({item}) => {
    return (
      <View
        style={styles.mealwrapper}
        onStartShouldSetResponder={() => {
          // console.log('aaaa');
          setmodalproduct(true);
        }}>
        <View style={styles.imagefix}>
          <Image
            source={item.image}
            style={{
              width: Dimensions.get('window').width / 3 - 20,
              height: Dimensions.get('window').width / 3 - 20,
              resizeMode: 'contain',
            }}
          />
        </View>
        <View style={styles.textfix}>
          <Text style={styles.mealtitle}>{item.title}</Text>
        </View>
      </View>
    );
  };

  /*************************************************************/
  function renderSearchIcon() {
    return <Icon name="search" style={styles.seacrchicon} />;
  }
  function renderPlusIcon() {
    return (
      <Icon name="minus" style={{...styles.seacrchicon, color: '#d0d0d0'}} />
    );
  }
  function renderMinIcon() {
    return (
      <Icon name="plus" style={{...styles.seacrchicon, color: '#d0d0d0'}} />
    );
  }
  /**************************MODAL FEEDBACK CONTENT***********************************/
  const modalContent = () => {
    return (
      <View style={styles.modalcontentwrapper}>
        <View style={styles.addwrapper}>
          <View style={styles.headtextwrapper}>
            <Text style={styles.headtext}>{langs.raterepr}</Text>
          </View>

          <View style={styles.imagewrapper}>
            <Image
              source={Images.feedback_icon}
              style={{
                width: Dimensions.get('window').width / 3,
                height: 60 * clerkimg,
              }}
              resizeMode="contain"
            />
            <Text style={styles.addtext}>أحمد محمد</Text>
          </View>
          <View style={styles.starswrapper}>
            <StarRating
              disabled={false}
              maxStars={5}
              rating={starCount}
              animation="rotate"
              emptyStarColor="#ededed"
              fullStarColor="#7bbd4a"
              selectedStar={(rating) => onStarRatingPress(rating)}
            />
          </View>

          <View style={styles.inputwrppermodal}>
            <Text style={styles.inputtext}>{langs.optiontext}</Text>
            <TextInput
              placeholder={langs.rateplace}
              style={styles.inputmodal}
              onChangeText={() => {}}
              placeholderTextColor="#a4a4a4"
              multiline={true}
            />
          </View>
        </View>
      </View>
    );
  };
  /**************************MODAL PRODUCT CONTENT***********************************/
  const sideItems = () => {
    return (
      <View style={styles.sideitemwrapper}>
        <View style={styles.rightside}>
          <Icon name="check" />
          <Text style={styles.nameside}>بيبسي صغير</Text>
        </View>
        <View>
          <Text style={styles.priceside}>4 ريال</Text>
        </View>
      </View>
    );
  };

  const modalProductContent = () => {
    return (
      <View style={styles.modalcontentwrapper}>
        <View style={styles.addwrapper}>
          <TouchableOpacity style={styles.wishwrapper} onPress={() => {}}>
            <Icon name="heart" style={[styles.seacrchicon, styles.hearticon]} />
          </TouchableOpacity>
          <Swiper
            style={styles.swiperwrapper}
            showsButtons={false}
            showsPagination={true}
            autoplay={true}
            dot={
              <View
                style={{
                  backgroundColor: '#d3d3d3',
                  ...styles.dotslider,
                }}
              />
            }
            activeDot={
              <View
                style={{
                  backgroundColor: '#7bbd4a',
                  ...styles.dotslider,
                }}
              />
            }>
            <View styles={styles.slide1}>
              <Image
                source={require('../../../assets/images/popup_image.png')}
                style={{
                  ...styles.imagecenter,
                  width: 200,
                  height: 100 * popup_image,
                }}
                resizeMode="contain"
              />
            </View>
            <View styles={styles.slide1}>
              <Image
                source={require('../../../assets/images/popup_image.png')}
                style={{
                  ...styles.imagecenter,
                  width: 200,
                  height: 100 * popup_image,
                }}
                resizeMode="contain"
              />
            </View>
          </Swiper>

          <View style={styles.wrapperfix}>
            <View style={styles.prodnamewrapper}>
              <View>
                <Text style={styles.prodname}>وجبة فروج</Text>
              </View>
              <View>
                <Text style={styles.prodprice}>35 ريال</Text>
              </View>
            </View>

            <View style={styles.boxeswrapper}>
              <View style={styles.boxone}>
                <Text style={styles.boxtext}>وجبات خاصة</Text>
              </View>
              <View style={{...styles.boxone, backgroundColor: '#f0c632'}}>
                <Icon name="clock" solid style={styles.boxicon} />
                <Text style={{...styles.boxtext, color: '#fff'}}>
                  30 - 20 دقيقه
                </Text>
              </View>
            </View>

            <View>
              <Text style={styles.prodescription}>
                وجبة الفروج المشوي الطازج - فرخة كاملة + بطاطس مقلية + بيبسي +
                طحينة
              </Text>
            </View>

            <View style={styles.sideswrapper}>
              <Text style={styles.sidetext}>{langs.sideitem}</Text>
            </View>
            {sideItems()}
            {sideItems()}
            {sideItems()}
            {sideItems()}
            {sideItems()}
            {sideItems()}
            {sideItems()}
            {sideItems()}
            {sideItems()}
            {sideItems()}
          </View>
        </View>
      </View>
    );
  };
  /*************************MODAL FEEDBACK************************************/
  const modalFeedback = () => {
    return (
      <Modal
        visible={modalOpen}
        animationType="slide"
        transparent={true}
        onRequestClose={() => {
          alert('Modal closed.');
        }}>
        <View style={styles.modalwrapper}>
          <TouchableOpacity
            onPress={() => setmodalOpen(false)}
            style={styles.iconmodalwrapper}>
            <Image
              source={Images.close_icon}
              // style={{width: 30, height: 30}}
              resizeMode="contain"
            />
          </TouchableOpacity>
          <ScrollView
            style={{...styles.innermodalwrapper, paddingVertical: 20}}>
            {modalContent()}
            <View style={styles.btmcontentModal}>
              <AppButton
                wrapperStyle={styles.appbutton}
                title={langs.sendbtn}
                onPress={() => {
                  // navigation.navigate('Home');
                }}
              />
            </View>
          </ScrollView>
        </View>
      </Modal>
    );
  };
  /*****************MODAL PRODUCT********************************************/

  const modalProduct = () => {
    return (
      <Modal
        visible={modalproduct}
        animationType="fade"
        transparent={true}
        onRequestClose={() => {
          alert('Modal closed.');
        }}>
        <View style={styles.modalwrapper}>
          <TouchableOpacity
            onPress={() => setmodalproduct(false)}
            style={styles.iconmodalwrapper}>
            <Image source={Images.close_icon} resizeMode="contain" />
          </TouchableOpacity>
          <ScrollView style={[styles.innermodalwrapper, styles.productwrapper]}>
            {modalProductContent()}
            <View style={styles.btmcontentModal}>
              <Input
                bordered
                placeholder="1"
                placeholderPosition="center"
                placeholderTextColor="#4f4e4e"
                renderIconRight={renderPlusIcon}
                renderIconLeft={renderMinIcon}
                keyboardType={'numeric'}
                style={styles.inputfix}
                wrapperStyle={{...styles.wrapperinput, ...styles.inputmoda}}
              />
              <AppButton
                wrapperStyle={styles.appbutton}
                title={langs.addbtn}
                onPress={() => {
                  navigation.navigate('Cart');
                  setmodalproduct(false);
                }}
              />
            </View>
          </ScrollView>
        </View>
      </Modal>
    );
  };

  /*************************************************************/
  return (
    <Container>
      <Content style={styles.conatiner}>
        {modalFeedback()}
        {modalProduct()}
        <View style={styles.tophead}>
          <Image
            source={Images.main_logo}
            style={{height: 70 * ratio}}
            resizeMode="contain"
          />
          <View style={styles.icons}>
            <View style={[styles.iconwrapper, styles.iconoverride]}>
              <Icon name="bell" solid style={styles.icon} />
              <View style={styles.countwrapper}>
                <Text style={styles.count}>2</Text>
              </View>
            </View>
            <View style={styles.iconwrapper}>
              <Icon name="shopping-basket" style={styles.icon} />
              <View style={[styles.countwrapper, styles.bagcolor]}>
                <Text style={styles.count}>2</Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.inputwrapper}>
          <Input
            bordered
            placeholder={langs.homeplacholder}
            placeholderPosition="right"
            placeholderTextColor="#c0c0c0"
            renderIconRight={renderSearchIcon}
            style={styles.inputfix}
            wrapperStyle={styles.wrapperinput}
            autoFocus={true}
          />
        </View>
        <View style={styles.catswrapper}>
          <FlatList
            data={CATEGORIES}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
          />
        </View>
        <View style={styles.prodwrapper}>
          <FlatList
            data={MEALS}
            renderItem={renderMeal}
            keyExtractor={(item) => item.id}
            numColumns={3}
          />
        </View>
      </Content>
      <Footerdef />
    </Container>
  );
};

export default HomeScreen;
