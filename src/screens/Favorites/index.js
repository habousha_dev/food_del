import {Container, Content} from 'native-base';
import React from 'react';
import {Animated, Image, Text, TouchableOpacity, View} from 'react-native';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Footerdef from '../../components/footer';
import HeaderCust from '../../components/Headers/Header';
import {Images} from '../../constants/Images';
import langs from '../../langs/ar';
import styles from './styles';

/*************************************************************/

/*************************************************************/
const renderProduct = () => {
  /*************************************************************/

  const _renderRightAction = (icon, color, backgroundColor, x, progress) => {
    const trans = progress.interpolate({
      inputRange: [0, 1],
      outputRange: [x, 0],
    });

    return (
      <Animated.View style={{flex: 1, transform: [{translateX: trans}]}}>
        <TouchableOpacity style={styles.icondeletewrapper}>
          <Icon name="trash-alt" solid style={styles.icondelete} />
        </TouchableOpacity>
      </Animated.View>
    );
  };

  const _renderRightActions = (progress) => (
    <View style={{width: 100, flexDirection: 'row'}}>
      {_renderRightAction(
        'trash',
        '#ffffff',
        '#rgb(255, 65, 65)',
        64,
        progress,
      )}
    </View>
  );

  _updateRef = (ref) => {
    _swipeableRow = ref;
  };
  /*************************************************************/
  return (
    <View style={styles.prodswrapper}>
      <Swipeable
        ref={_updateRef}
        friction={2}
        rightThreshold={40}
        renderRightActions={_renderRightActions}>
        <TouchableOpacity style={styles.prodwrapper}>
          <View style={styles.prodcontainer}>
            <View style={styles.imagecontainer}>
              <Image source={Images.cart_icon} style={styles.image} />
            </View>
            <View style={styles.titlecontainer}>
              <Text style={styles.text}>وجبة فروج</Text>
            </View>
          </View>
          <View style={styles.price}>
            <Text style={styles.pricetext}>35 {langs.riyal}</Text>
          </View>
        </TouchableOpacity>
      </Swipeable>
      <Swipeable
        ref={_updateRef}
        friction={2}
        rightThreshold={40}
        renderRightActions={_renderRightActions}>
        <TouchableOpacity style={styles.prodwrapper}>
          <View style={styles.prodcontainer}>
            <View style={styles.imagecontainer}>
              <Image source={Images.cart_icon} style={styles.image} />
            </View>
            <View style={styles.titlecontainer}>
              <Text style={styles.text}>وجبة فروج</Text>
            </View>
          </View>
          <View style={styles.price}>
            <Text style={styles.pricetext}>35 {langs.riyal}</Text>
          </View>
        </TouchableOpacity>
      </Swipeable>
      <Swipeable
        ref={_updateRef}
        friction={2}
        rightThreshold={40}
        renderRightActions={_renderRightActions}>
        <TouchableOpacity style={styles.prodwrapper}>
          <View style={styles.prodcontainer}>
            <View style={styles.imagecontainer}>
              <Image source={Images.cart_icon} style={styles.image} />
            </View>
            <View style={styles.titlecontainer}>
              <Text style={styles.text}>وجبة فروج</Text>
            </View>
          </View>
          <View style={styles.price}>
            <Text style={styles.pricetext}>35 {langs.riyal}</Text>
          </View>
        </TouchableOpacity>
      </Swipeable>
      <Swipeable
        ref={_updateRef}
        friction={2}
        rightThreshold={40}
        renderRightActions={_renderRightActions}>
        <TouchableOpacity style={styles.prodwrapper}>
          <View style={styles.prodcontainer}>
            <View style={styles.imagecontainer}>
              <Image source={Images.cart_icon} style={styles.image} />
            </View>
            <View style={styles.titlecontainer}>
              <Text style={styles.text}>وجبة فروج</Text>
            </View>
          </View>
          <View style={styles.price}>
            <Text style={styles.pricetext}>35 {langs.riyal}</Text>
          </View>
        </TouchableOpacity>
      </Swipeable>
      <Swipeable
        ref={_updateRef}
        friction={2}
        rightThreshold={40}
        renderRightActions={_renderRightActions}>
        <TouchableOpacity style={styles.prodwrapper}>
          <View style={styles.prodcontainer}>
            <View style={styles.imagecontainer}>
              <Image source={Images.cart_icon} style={styles.image} />
            </View>
            <View style={styles.titlecontainer}>
              <Text style={styles.text}>وجبة فروج</Text>
            </View>
          </View>
          <View style={styles.price}>
            <Text style={styles.pricetext}>35 {langs.riyal}</Text>
          </View>
        </TouchableOpacity>
      </Swipeable>
      <Swipeable
        ref={_updateRef}
        friction={2}
        rightThreshold={40}
        renderRightActions={_renderRightActions}>
        <TouchableOpacity style={styles.prodwrapper}>
          <View style={styles.prodcontainer}>
            <View style={styles.imagecontainer}>
              <Image source={Images.cart_icon} style={styles.image} />
            </View>
            <View style={styles.titlecontainer}>
              <Text style={styles.text}>وجبة فروج</Text>
            </View>
          </View>
          <View style={styles.price}>
            <Text style={styles.pricetext}>35 {langs.riyal}</Text>
          </View>
        </TouchableOpacity>
      </Swipeable>
    </View>
  );
};
const FavoritesScreen = ({navigation}) => {
  //   const {navigation} = props;
  return (
    <Container>
      <HeaderCust title={`${langs.favheader}`} />
      <Content style={styles.conatiner}>{renderProduct()}</Content>
      <Footerdef />
    </Container>
  );
};

export default FavoritesScreen;
