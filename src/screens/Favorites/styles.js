/*************************************************************/

import {ScaledSheet} from 'react-native-size-matters';
import {MAIN_COLOR, TEXT_COLOR} from '../../constants/Colors';
import {FONTS} from '../../constants/Fonts';
/*************************************************************/

const {FrutigerLight, Frutigerroman, FrutigerBold, FrutigerBlack} = FONTS;
/*************************************************************/

const styles = ScaledSheet.create({
  conatiner: {
    flex: 1,
    backgroundColor: '#fff',
    // marginTop: 5,
    // paddingVertical: '100@s',
  },
  prodwrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    paddingVertical: '10@vs',
    paddingHorizontal: '10@s',
    borderBottomColor: '#f0f0f0',
    borderBottomWidth: 1,
  },
  prodswrapper: {
    // paddingTop: '5@s',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    backgroundColor: '#fff',
    marginTop: '5@s',
  },
  prodcontainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  price: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  pricetext: {
    fontFamily: Frutigerroman,
    color: MAIN_COLOR,
  },
  text: {
    fontFamily: FrutigerBold,
    fontSize: '15@s',
    color: TEXT_COLOR,
  },
  image: {
    width: '45@s',
    height: '45@s',
    borderRadius: 23,
  },
  imagecontainer: {
    backgroundColor: '#fff',
    width: '60@s',
    height: '60@s',
    borderRadius: 40,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: '10@s',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  titlecontainer: {
    paddingHorizontal: '10@s',
  },
  icondeletewrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    backgroundColor: 'rgb(255, 65, 65)',
  },
  icondelete: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: '#fff',
    fontSize: '20@s',
  },
});
export default styles;
