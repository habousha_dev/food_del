import {Container} from 'native-base';
import React from 'react';
import {
  Keyboard,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {AppButton} from '../../components/AppButton';
import HeaderCust from '../../components/Headers/Header';
import {Input} from '../../components/Input';
import langs from '../../langs/ar';
import styles from './styles';

/*************************************************************/

/*************************************************************/

const EditAccountScreen = ({props}) => {
  return (
    <Container>
      <HeaderCust title={` ${langs.editbtn}`} style={styles.headfix} />
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View style={styles.conatiner}>
          <View style={styles.inputwrraper}>
            <View style={styles.textwrapper}>
              <Text style={styles.text}>{langs.fullname}</Text>
            </View>
            <Input
              placeholder="TEST"
              underlined
              style={styles.input}
              placeholderPosition="right"
              placeholderTextColor="#4f4e4e"
            />
          </View>
          <View style={styles.inputwrraper}>
            <View style={styles.textwrapper}>
              <Text style={styles.text}>{langs.email}</Text>
            </View>
            <Input
              placeholder="DD@AA.CC"
              underlined
              style={styles.input}
              placeholderPosition="right"
              placeholderTextColor="#4f4e4e"
              keyboardType={'email-address'}
            />
          </View>
          <View style={styles.inputwrraper}>
            <View style={styles.textwrapper}>
              <Text style={styles.text}>{langs.phone}</Text>
            </View>
            <Input
              placeholder="015200365222222"
              underlined
              style={styles.input}
              placeholderPosition="right"
              placeholderTextColor="#4f4e4e"
              keyboardType={'phone-pad'}
            />
          </View>
          <TouchableOpacity style={styles.forgetwrapper}>
            <Text style={styles.forgettext}>{langs.changepassword}</Text>
            <Icon name="arrow-left" style={styles.forgeticon} />
          </TouchableOpacity>
          <View style={styles.btnwrapper}>
            <AppButton
              title={langs.changebtn}
              wrapperStyle={styles.appbutton}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    </Container>
  );
};

export default EditAccountScreen;
