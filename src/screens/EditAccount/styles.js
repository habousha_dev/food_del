/*************************************************************/

import {ScaledSheet} from 'react-native-size-matters';
import {MAIN_COLOR} from '../../constants/Colors';
import {FONTS} from '../../constants/Fonts';
/*************************************************************/

const {FrutigerLight, Frutigerroman, FrutigerBold, FrutigerBlack} = FONTS;
/*************************************************************/

const styles = ScaledSheet.create({
  conatiner: {
    flex: 1,
    // paddingVertical: '100@s',
    backgroundColor: '#fff',
    paddingHorizontal: '20@s',
    // marginTop: '5@s',
  },
  text: {
    // fontFamily: FrutigerBlack,
    color: '#a4a4a4',
    fontFamily: Frutigerroman,
    fontSize: '14@s',
  },
  input: {
    fontSize: '12@s',
    fontFamily: FrutigerBold,
    color: '#4f4e4e',
    paddingBottom: '7@s',
  },
  inputwrraper: {
    marginBottom: '20@s',
  },
  headfix: {
    marginBottom: '20@s',
  },
  forgetwrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    marginTop: '10@s',
  },
  forgettext: {
    fontSize: '15@s',
    color: MAIN_COLOR,
    fontFamily: Frutigerroman,
  },
  forgeticon: {
    fontSize: '12@s',
    color: MAIN_COLOR,
    paddingHorizontal: '8@s',
  },
  appbutton: {
    marginHorizontal: 0,
    display: 'flex',
  },
  btnwrapper: {
    flex: 1,
    // backgroundColor: 'red',
    justifyContent: 'flex-end',
    marginBottom: '20@vs',
  },
});
export default styles;
