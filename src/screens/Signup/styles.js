/*************************************************************/

import { ScaledSheet } from 'react-native-size-matters';
import { TEXT_COLOR } from '../../constants/Colors';
import { FONTS } from '../../constants/Fonts';
/*************************************************************/

const {FrutigerLight, Frutigerroman, FrutigerBold, FrutigerBlack} = FONTS;
/*************************************************************/

const styles = ScaledSheet.create({
  conatiner: {
    flex: 1,
    backgroundColor: '#fff',
  },
  wrapper: {
    paddingHorizontal: '25@s',
  },
  backbtn: {
    backgroundColor: '#rgb(216, 216, 216)',
    paddingVertical: '12@s',
    paddingHorizontal: '18@s',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'flex-end',
    marginTop: '20@vs',
    alignItems: 'center',
    borderTopStartRadius: '30@s',
    borderBottomLeftRadius: '30@s',
  },
  backicon: {
    color: '#ffffff',
    fontSize: '18@s',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imagewrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: '20@vs',
  },
  socialwrapper: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: '25@vs',
  },
  signintext: {
    textAlign: 'center',
    color: TEXT_COLOR,
    fontSize: '20@s',
    fontFamily: FrutigerBold,
  },
  signinwrapper:{
    marginVertical:'10@vs'
  },
  rights: {
    textAlign: 'center',
    color: '#a4a4a4',
    fontFamily: FrutigerLight,
    fontSize: '11@s',
  },
  iconrender: {
    color: '#a4a4a4',
    fontSize: '15@s',
    paddingBottom: '8@vs',
  },
  inputfix: {
    paddingBottom: '8@vs',
    fontSize: '12@s',
    fontFamily: Frutigerroman,
  },
  inputwrraper: {
    marginVertical: '15@s',
  },
  appbutton: {
    marginHorizontal: 0,
    paddingVertical: '10@vs',
    marginVertical:'30@vs'
  },
});
export default styles;
