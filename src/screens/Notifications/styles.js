/*************************************************************/

import {ScaledSheet} from 'react-native-size-matters';
import {
  BACKGROUND_COLOR,
  BORDER_COLOR,
  DATE_COLOR,
  TEXT_COLOR,
} from '../../constants/Colors';
import {FONTS} from '../../constants/Fonts';
/*************************************************************/

const {FrutigerLight, Frutigerroman, FrutigerBold, FrutigerBlack} = FONTS;
/*************************************************************/

const styles = ScaledSheet.create({
  conatiner: {
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    elevation: 8,
  },
  prodcontainer: {
    flex: 1,
    backgroundColor: BACKGROUND_COLOR,
    paddingHorizontal: '20@s',
    paddingVertical: '10@vs',
    borderBottomColor: BORDER_COLOR,
    borderBottomWidth: 1,
  },
  rightcontainer: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingHorizontal: '20@s',
    alignItems: 'center',
  },
  imagecontainer: {
    backgroundColor: '#fff',
    width: '60@s',
    height: '60@s',
    borderRadius: 35,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: '10@s',
  },
  textcontainer: {
    paddingHorizontal: '10@s',
  },
  image: {
    width: '45@s',
    height: '45@s',
    borderRadius: 23,
  },
  datecontainer: {
    display: 'flex',
    alignItems: 'flex-end',
  },
  text: {
    color: TEXT_COLOR,
    fontSize: '12@s',
    fontFamily: FrutigerBold,
  },
  date: {
    color: DATE_COLOR,
    fontSize: '12@s',
    fontFamily: FrutigerBold,
  },
  // item: {
  //   height: 75,
  //   flexDirection: 'row',
  //   alignItems: 'center',
  //   paddingHorizontal: 15,
  // },

  // message: {
  //   color: 'gray',
  //   fontSize: 14,
  //   marginTop: 3,
  // },
  // rightAction: {
  //   flex: 1,
  //   alignItems: 'center',
  //   justifyContent: 'center',
  //   height: 75,
  //   color: 'red',
  // },
  icondeletewrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    backgroundColor: 'rgb(255, 65, 65)',
  },
  icondelete: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: '#fff',
    fontSize: '20@s',
  },
});

export default styles;
