import {Container, Content} from 'native-base';
import React from 'react';
import {Animated, Image, Text, TouchableOpacity, View} from 'react-native';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import Icon from 'react-native-vector-icons/FontAwesome5';
import HeaderCust from '../../components/Headers/Header';
import {Images} from '../../constants/Images';
import styles from './styles';
/*************************************************************/
const NotificationScreen = ({props}) => {
  /*************************************************************/
  const _renderRightAction = (icon, color, backgroundColor, x, progress) => {
    const trans = progress.interpolate({
      inputRange: [0, 1],
      outputRange: [x, 0],
    });

    return (
      <Animated.View style={{flex: 1, transform: [{translateX: trans}]}}>
        <TouchableOpacity style={styles.icondeletewrapper}>
          <Icon name="trash-alt" solid style={styles.icondelete} />
        </TouchableOpacity>
      </Animated.View>
    );
  };

  const _renderRightActions = (progress) => (
    <View style={{width: 100, flexDirection: 'row'}}>
      {_renderRightAction(
        'trash',
        '#ffffff',
        '#rgb(255, 65, 65)',
        64,
        progress,
      )}
    </View>
  );

  _updateRef = (ref) => {
    _swipeableRow = ref;
  };

  /*************************************************************/
  const renderProducts = () => {
    return (
      <View>
        <Swipeable
          ref={_updateRef}
          friction={2}
          rightThreshold={40}
          renderRightActions={_renderRightActions}>
          <TouchableOpacity style={styles.prodcontainer} onPress={() => {}}>
            <View style={styles.rightcontainer}>
              <View style={styles.imagecontainer}>
                <Image source={Images.notifProduct} style={styles.image} />
              </View>

              <View style={styles.textcontainer}>
                <Text style={styles.text}>
                  اطلب ساندوتش البرجر العربي مع البطاطس والبيبسي واحصل علي
                  بسبوسة
                </Text>
              </View>
            </View>

            <View style={styles.datecontainer}>
              <Text style={styles.date}>25 مارس 2019</Text>
            </View>
          </TouchableOpacity>
        </Swipeable>
        <Swipeable
          ref={_updateRef}
          friction={2}
          rightThreshold={40}
          renderRightActions={_renderRightActions}>
          <TouchableOpacity style={styles.prodcontainer} onPress={() => {}}>
            <View style={styles.rightcontainer}>
              <View style={styles.imagecontainer}>
                <Image source={Images.notifProduct} style={styles.image} />
              </View>

              <View style={styles.textcontainer}>
                <Text style={styles.text}>
                  اطلب ساندوتش البرجر العربي مع البطاطس والبيبسي واحصل علي
                  بسبوسة
                </Text>
              </View>
            </View>

            <View style={styles.datecontainer}>
              <Text style={styles.date}>25 مارس 2019</Text>
            </View>
          </TouchableOpacity>
        </Swipeable>
        <Swipeable
          ref={_updateRef}
          friction={2}
          rightThreshold={40}
          renderRightActions={_renderRightActions}>
          <TouchableOpacity style={styles.prodcontainer} onPress={() => {}}>
            <View style={styles.rightcontainer}>
              <View style={styles.imagecontainer}>
                <Image source={Images.notifProduct} style={styles.image} />
              </View>

              <View style={styles.textcontainer}>
                <Text style={styles.text}>
                  اطلب ساندوتش البرجر العربي مع البطاطس والبيبسي واحصل علي
                  بسبوسة
                </Text>
              </View>
            </View>

            <View style={styles.datecontainer}>
              <Text style={styles.date}>25 مارس 2019</Text>
            </View>
          </TouchableOpacity>
        </Swipeable>
      </View>
    );
  };
  /*************************************************************/
  return (
    <Container>
      <HeaderCust title={` قائمة التنبيهات ${2}`} />
      <Content style={styles.conatiner}>{renderProducts()}</Content>
    </Container>
  );
};

export default NotificationScreen;
