/*************************************************************/

import {ScaledSheet} from 'react-native-size-matters';
import {TEXT_COLOR} from '../../constants/Colors';
import {FONTS} from '../../constants/Fonts';
/*************************************************************/

const {FrutigerLight, Frutigerroman, FrutigerBold, FrutigerBlack} = FONTS;
/*************************************************************/

const styles = ScaledSheet.create({
  conatiner: {
    flex: 1,
    paddingTop: '20@vs',
    paddingHorizontal: '30@s',
  },
  mainwrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderBottomColor: '#e5e5e5',
    borderBottomWidth: 1,
    paddingBottom: '10@s',
    marginBottom: '20@vs',
    backgroundColor: '#fff',
  },
  rightwrapper: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  name: {
    color: '#a4a4a4',
    fontSize: '15@s',
    fontFamily: Frutigerroman,
  },
  address: {
    color: TEXT_COLOR,
    fontSize: '12@s',
    fontFamily: FrutigerLight,
  },
  phone: {
    color: TEXT_COLOR,
    fontSize: '14@s',
    fontFamily: FrutigerBold,
  },
  addresswrapper: {
    paddingVertical: '5@vs',
  },
  downbtm: {
    backgroundColor: '#rgb(123, 189, 74)',
    position: 'absolute',
    bottom: 20,
    // left: 20,
    marginHorizontal: '30@s',
    height: '50@s',
    width: '50@s',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
    // padding: '15@s',
    // borderRadius: '50%',
  },
  downicon: {
    fontSize: '20@s',
    color: '#fff',
  },
});
export default styles;
