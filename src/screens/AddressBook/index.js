import {Container, Content, Radio} from 'native-base';
import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import HeaderCust from '../../components/Headers/Header';
/*************************************************************/
import langs from '../../langs/ar';
import styles from './styles';

/*************************************************************/

const AddressBookScreen = ({navigation}) => {
  //   const [radioVal, setRadioVal] = useState('');
  const [isSelected, setSelection] = React.useState(false);
  const renderItem = () => {
    return (
      <TouchableOpacity style={styles.mainwrapper}>
        <View style={styles.rightwrapper}>
          <View style={styles.namewrapper}>
            <Text style={styles.name}>Test user</Text>
          </View>
          <View style={styles.addresswrapper}>
            <Text style={styles.address}>
              الأخيار، الجزيرة، الرياض 14251 2229، السعودية
            </Text>
          </View>
          <View style={styles.phonewrapper}>
            <Text style={styles.phone}>01000000000</Text>
          </View>
        </View>
        <View style={styles.iconwrapper}>
          <Radio color={'#f0ad4e'} selectedColor={'#5cb85c'} selected={true} />
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <Container>
      <HeaderCust title={` ${langs.addbookheader}`} style={styles.headfix} />
      <Content style={styles.conatiner}>
        {renderItem()}
        {renderItem()}
        {renderItem()}
        {renderItem()}
      </Content>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('AddNewAddress');
        }}
        style={styles.downbtm}>
        <Icon name="plus" style={styles.downicon} />
      </TouchableOpacity>
    </Container>
  );
};

export default AddressBookScreen;
