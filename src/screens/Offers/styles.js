/*************************************************************/

import {ScaledSheet} from 'react-native-size-matters';
import {FONTS} from '../../constants/Fonts';
/*************************************************************/

const {FrutigerLight, Frutigerroman, FrutigerBold, FrutigerBlack} = FONTS;
/*************************************************************/

const styles = ScaledSheet.create({
  conatiner: {
    flex: 1,
    // paddingTop: 20,
  },
  maincontainer: {
    paddingTop: '10@s',
    marginHorizontal: '5@s',
  },
  imagecontainer: {
    marginBottom: '10@s',
  },
});
export default styles;
