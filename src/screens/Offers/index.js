import {Container, Content} from 'native-base';
import React from 'react';
import {Dimensions, Image, TouchableOpacity, View} from 'react-native';
import Footerdef from '../../components/footer';
import HeaderCust from '../../components/Headers/Header';
import {Images} from '../../constants/Images';
import langs from '../../langs/ar';
import styles from './styles';
/*************************************************************/
const win = Dimensions.get('window');
const ratio = win.width / 704; //704 is actual image width

/*************************************************************/

const renderOffers = (navigation) => {
  return (
    <View style={styles.maincontainer}>
      <TouchableOpacity
        style={styles.imagecontainer}
        onPress={() => {
          // navigation.navigate('Privacy');
        }}>
        <Image
          source={Images.offer_first}
          style={{
            width: win.width,
            height: 262 * ratio,
          }}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <TouchableOpacity style={styles.imagecontainer} onPress={() => {}}>
        <Image
          source={Images.offer_second}
          style={{
            width: win.width,
            height: 262 * ratio,
          }}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <TouchableOpacity style={styles.imagecontainer} onPress={() => {}}>
        <Image
          source={Images.offer_third}
          style={{
            width: win.width,
            height: 262 * ratio,
          }}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.imagecontainer}
        onPress={() => {
          // navigation.navigate('Privacy');
        }}>
        <Image
          source={Images.offer_first}
          style={{
            width: win.width,
            height: 262 * ratio,
          }}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <TouchableOpacity style={styles.imagecontainer} onPress={() => {}}>
        <Image
          source={Images.offer_second}
          style={{
            width: win.width,
            height: 262 * ratio,
          }}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <TouchableOpacity style={styles.imagecontainer} onPress={() => {}}>
        <Image
          source={Images.offer_third}
          style={{
            width: win.width,
            height: 262 * ratio,
          }}
          resizeMode="contain"
        />
      </TouchableOpacity>
    </View>
  );
};
const OffersScreen = ({navigation}) => {
  return (
    <Container>
      <HeaderCust title={`${langs.offersheader}`} />
      <Content style={styles.conatiner}>{renderOffers(navigation)}</Content>
      <Footerdef />
    </Container>
  );
};

export default OffersScreen;
