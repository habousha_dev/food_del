import {Container, Content} from 'native-base';
import React from 'react';
import {Dimensions, Image, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {AppButton} from '../../components/AppButton';
import {Images} from '../../constants/Images';
import langs from '../../langs/ar';
import styles from './styles';
const win = Dimensions.get('window');
const ratio = win.width / 542; //542 is actual image width

/*************************************************************/

/*************************************************************/

const StatusScreen = ({navigation}) => {
  return (
    <Container style={styles.maincontainer}>
      <Content>
        <View style={styles.imagecontainer}>
          <Image
            source={Images.status}
            style={{
              width: win.width,
              height: 500 * ratio,
            }}
            resizeMode="contain"
          />
        </View>
        <View style={styles.underwaycontainer}>
          <Text style={styles.underway}>{langs.under}</Text>
        </View>
        <View style={styles.throughcontainer}>
          <Text style={styles.through}>{langs.through}</Text>
        </View>
        <View style={styles.timecontainer}>
          <Icon name="clock" style={styles.icon} />
          <Text style={styles.time}>30 {langs.minutes}</Text>
        </View>
        <View style={styles.buttonWrapper}>
          <AppButton
            title={langs.backhome}
            wrapperStyle={styles.wrapperStyle}
            onPress={() => {
              navigation.navigate('Home');
            }}
          />
        </View>
      </Content>
    </Container>
  );
};

export default StatusScreen;
