/*************************************************************/

import {Dimensions} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {MAIN_COLOR, TEXT_COLOR, TEXT_PRIMARY} from '../../constants/Colors';
import {FONTS} from '../../constants/Fonts';
/*************************************************************/
const win = Dimensions.get('window');

const {FrutigerLight, Frutigerroman, FrutigerBold, FrutigerBlack} = FONTS;
/*************************************************************/

const styles = ScaledSheet.create({
  maincontainer: {
    // backgroundColor: '#fff', paddingHorizontal: 50
    flex: 1,
    display: 'flex',
    alignItems: 'center',
    backgroundColor: '#fff',
    // paddingTop: '50@vs',
    // paddingHorizontal: '20@s',
  },
  conatiner: {},
  imagecontainer: {
    marginBottom: '20@s',
    paddingTop: '50@vs',

    // alignSelf: 'center',
    // paddingHorizontal: '20@s',
  },
  underway: {
    fontFamily: FrutigerBold,
    fontSize: '30@s',
    color: MAIN_COLOR,
  },
  underwaycontainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  through: {
    fontFamily: FrutigerBold,
    fontSize: '13@s',
    color: TEXT_PRIMARY,
  },
  time: {
    fontFamily: FrutigerBlack,
    fontSize: '12@s',
    color: TEXT_COLOR,
  },
  timecontainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: '20@s',
  },
  icon: {
    color: MAIN_COLOR,
    fontSize: '20@s',
  },
  throughcontainer: {
    paddingVertical: '15@vs',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wrapperStyle: {
    backgroundColor: '#f0c632',
    borderColor: '#f0c632',
    // marginHorizontal: 0,
  },
  buttonWrapper: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: '20@vs',
  },
});
export default styles;
