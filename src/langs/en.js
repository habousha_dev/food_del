export default {
    // WALKTHROUGH
    order: 'I order to eat with ease',
    wehave: 'We have an interesting menu',
    skip: 'Skip',
    deliver: 'We get your order quickly',
    wehave: 'We have delivery coverage all over the Kingdom',
    regnow: 'Register an account now',
    delicios: 'And listen to delicious meals and strong shows',
    register: 'Register a new account',
    enteremail: 'Enter your email address',
    login: 'Login',
    // WALKTHROUGH
    rights: 'All rights Reserved , TASWAK CO.',
    ksa: 'Kingdom of Saudi Arabia',
  
    // LOGIN && REGISTER
    signwith: 'Log in to your account with',
    withgoogle: 'Google account',
    withfacebook: 'Facebook account',
    or_by: 'Or by',
    email: 'E-mail',
    password: 'Password',
    regbtn: 'Register',
    forget: 'Did you forget your password ?',
    contbtn: 'Continue',
    // LOGIN && REGISTER
  
    // HOME
    homeplacholder: 'What you want to eat ?',
    addbtn: 'Add to cart',
    sideitem: 'Side items',
    home: 'Home',
    // HOME
  
    // NOTIFICATION && CART && CHECKOUT
    notiheader: 'List of alerts',
    cartheader: 'Cart',
    checkheader: 'Complete the request',
    stockhint: 'Sorry, the minimum order is 35 riyals',
    coupoun: 'Add the discount coupon',
    coupnplace: 'Write down your discount coupon code here',
    ordertotal: 'Total order',
    orderdel: 'Delivery cost',
    orderhowtopay: 'Payment method',
    orderaddress: 'Delivery Address',
    vat: 'Value added tax.',
    finaltotal: 'Total',
    finishbtn: 'Complete the order',
    paytype: 'Select a payment method',
    timeorder: 'Select the time to receive the order',
    riyal: 'Riyal',
  
    money: 'monetary',
    visa: 'Visa or Master Card',
    now: 'immediately',
    custdate: 'Select a time',
    // NOTIFICATION && CART && CHECKOUT
  
    // STATUS && FEEDBACK-POPUP && FAVOURIETES && OFFERS && ORDERS
    orderheader: 'Request No.',
    under: 'Your order is under implementation',
    through: 'The order will be received within',
    minutes: 'minute',
    backhome: 'Back to home',
    optiontext: 'optional',
    sendbtn: 'Send',
    raterepr: 'How do you rate the delivery representative?',
    rateplace: 'Add any other comments on the service',
    favheader: 'My favourite',
    offersheader: 'Offers',
    ordersheader: 'Orders',
    ordernum: 'Order number',
    orderdet: 'View order details',
    orderstatustab: 'Order status',
    orderdettab: 'Order details',
    returnbtn: 'Re-order',
  
    firststep: 'We received your request, dear',
    firststepover: 'Trying to be late for you',
    secondstep: 'Order your kitchen',
    secondstepover: 'The cook leaves to his hand and prepares you for your order',
    thirdstep: 'The cook finished',
    thirdstepover: 'Young people are fills up your order',
    fourthstep: 'Your request for delivery',
    fourthstepover: 'Fly the delivery representative to deliver your order',
    fififthstep: 'You received the order',
    fififstepover: 'A thousand well-being on your heart',
    // STATUS && FEEDBACK-POPUP && FAVOURIETES && OFFERS && ORDERS
  
    //MY ACCOUNT && EDIT ACCOUNT && ADDRESS BOOK
    accheader: 'My account',
    editbtn: 'Modify account data',
    addstack: 'Address Book',
    langchange: 'Change the language',
    notify: 'Alerts',
    help: 'Help',
    instruc: 'Terms of service',
    privacy: 'Privacy policy',
    rate: 'Application evaluation',
    sharewith: 'Share the app with your friends',
    logoutbtn: 'Log out',
  
    fullname: 'Full name',
    phone: 'Mobile number',
    changepassword: 'Change Password',
  
    addbookheader: 'Address Book',
    addnewaddress: 'Add a new address',
  
    country: 'Country',
    zone: 'Region',
    city: 'City',
    route: 'Street name',
    bulidnum: 'Building number',
    flatnum: 'Apartment number',
    phonenum: 'Mobile number',
    savebtn: 'Save the address',
    changebtn: 'Saving changes',
    helpheader: 'Help',
    privacyheader: 'Privacy policy',
  
    //MY ACCOUNT && EDIT ACCOUNT ADDRESS BOOK
  };
  