export default {
  // WALKTHROUGH
  order: 'أطلب أكلك بكل سهولة',
  wehave: 'لدينا قائمه طعام مشوقه',
  skip: 'تخطى',
  deliver: 'نوصلك طلبك على وجة السرعة',
  wehave: 'لدينا تغطية توصيل لجميع أنحاء المملكة',
  regnow: 'سجل حساب الآن',
  delicios: 'واستسمع بأشهى الوجبات وأقوى العروض',
  register: 'تسجيل حساب جديد',
  enteremail: 'أدخل بريدك الإلكترونى',
  login: 'دخول',
  // WALKTHROUGH
  rights: 'جميع الحقوق محفوظة. مؤسسة تسوق لتقنية المعلومات',
  ksa: 'المملكةالعربية السعودية',

  // LOGIN && REGISTER
  signwith: 'سجل دخول لحسابك عن طريق',
  withgoogle: 'حساب جوجل',
  withfacebook: 'حساب فيسبوك',
  or_by: 'أو عن طريق',
  email: 'البريد الإلكترونى',
  password: 'كلمة المرور',
  regbtn: 'تسجيل',
  forget: 'هل نسيت كلمة المرور',
  contbtn: 'متابعة',
  // LOGIN && REGISTER

  // HOME
  homeplacholder: 'تبغى تاكل إيش ؟',
  addbtn: 'أضف إلى السلة',
  sideitem: 'الأصناف الجانبية',
  home: 'الرئيسية',
  // HOME

  // NOTIFICATION && CART && CHECKOUT
  notiheader: 'قائمة التنبيهات',
  cartheader: 'سلة الشراء',
  checkheader: 'إتمام الطلب',
  stockhint: 'عفواً الحد الأدنى للطلبات 35 ريال',
  coupoun: 'أضف كوبون التخفيض',
  coupnplace: 'أكتب كود كوبون التخفيض هنا',
  ordertotal: 'إجمالى الطلب',
  orderdel: 'تكلفة التوصيل',
  orderhowtopay: 'وسيلة الدفع',
  orderaddress: 'عنوان التوصيل',
  vat: 'ضريبة القيمة المضافة',
  finaltotal: 'الإجمالى',
  finishbtn: 'إتمام الطلب',
  paytype: 'حدد طريقة الدفع',
  timeorder: 'حدد وقت إستلام الطلب',
  riyal: 'ريال',

  money: 'نقدى',
  visa: 'فيزا أو ماستر كارد',
  now: 'فورى',
  custdate: 'تحديد وقت',
  // NOTIFICATION && CART && CHECKOUT

  // STATUS && FEEDBACK-POPUP && FAVOURIETES && OFFERS && ORDERS
  orderheader: 'الطلب رقم',
  under: 'طلبكم قيد التنفيذ',
  through: 'سيتم توصل الطلب خلال',
  minutes: 'دقيقة',
  backhome: 'الرجوع للرئيسية',
  optiontext: 'إختيارى',
  sendbtn: 'إرسال',
  raterepr: 'ما تقييمك لمندوب التوصيل ؟',
  rateplace: 'أضف أى ملاحظات أخرى على الخدمة',
  favheader: 'مفضلتى',
  offersheader: 'العروض',
  ordersheader: 'طلباتى',
  ordernum: 'رقم الطلب',
  orderdet: 'عرض تفاصيل الطلب',
  orderstatustab: 'حالة الطلب',
  orderdettab: 'تفاصيل الطلب',
  returnbtn: 'إعادة الطلب',

  firststep: 'إستلمنا طلبك يا غالى',
  firststepover: 'نحاول ما نتأخر عليك',
  secondstep: 'طلبك بالمطبخ',
  secondstepover: 'الطباخ تارك الى بايده وبيجهزلك طلبك',
  thirdstep: 'خلص الطباخ',
  thirdstepover: 'الشباب يعبون طلبك',
  fourthstep: 'طلبك عالتوصيل',
  fourthstepover: 'طاير مندوب التوصيل ليوصلك طلبك',
  fififthstep: 'وصلك الطلب',
  fififstepover: 'ألف عافيه على قلبك',
  // STATUS && FEEDBACK-POPUP && FAVOURIETES && OFFERS && ORDERS

  //MY ACCOUNT && EDIT ACCOUNT && ADDRESS BOOK
  accheader: 'حسابى',
  editbtn: 'تعديل بيانات الحساب',
  addstack: 'دفتر العناوين',
  langchange: 'تغيير اللغة',
  notify: 'التنبيهات',
  help: 'المساعدة',
  instruc: 'شروط الخدمة',
  privacy: 'سياسة الخصوصية',
  rate: 'تقييم التطبيق',
  sharewith: 'شارك التطبيق مع أصدقائك',
  logoutbtn: 'تسجيل خروج',

  fullname: 'الإسم بالكامل',
  phone: 'رقم الجوال',
  changepassword: 'تغيير كلمة المرور',

  addbookheader: 'دفتر العناوين',
  addnewaddress: 'إضافه عنوان جديد',

  country: 'البلد',
  zone: 'المنطقة',
  city: 'المدينة',
  route: 'اسم الشارع',
  bulidnum: 'رقم البناية',
  flatnum: 'رقم الشقه',
  phonenum: 'رقم الجوال',
  savebtn: 'حفظ العنوان',
  changebtn: 'حفظ التغييرات',
  helpheader: 'المساعدة',
  privacyheader: 'سياسة الخصوصية',

  //MY ACCOUNT && EDIT ACCOUNT ADDRESS BOOK
};
