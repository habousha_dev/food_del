export const MAIN_COLOR = '#7bbd4a';

export const HEADER_COLOR = '#4f4e4e';

export const TEXT_COLOR = '#4f4e4e';
export const TEXT_PRIMARY = '#9b9b9b';

export const BACKGROUND_COLOR = '#f6faf2';

export const DATE_COLOR = '#9d9d9d';
export const BORDER_COLOR = '#ebebeb';

export const ICONS_COLOR = '#d0d0d0';
