const FrutigerBlack = 'fblack';
const FrutigerBold = 'fbold';
const FrutigerLight = 'flight';
const Frutigerroman = 'froman';


export const FONTS = {
    Frutigerroman: Frutigerroman,
    FrutigerLight: FrutigerLight,
    FrutigerBold: FrutigerBold,
    FrutigerBlack: FrutigerBlack,
};
