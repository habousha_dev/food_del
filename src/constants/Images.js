//  images;
export const Images = {
  privacy: require('../../assets/images/privacy_image.png'),
  cart_icon: require('../../assets/images/cart_icon.png'),
  notifProduct: require('../../assets/images/notification_icon.png'),
  status: require('../../assets/images/status_image.png'),
  offer_first: require('../../assets/images/offer_first.png'),
  offer_second: require('../../assets/images/offer_second.png'),
  offer_third: require('../../assets/images/offer_third.png'),
  signin_logo: require('../../assets/images/signin_logo.png'),
  googleicon: require('../../assets/images/googleicon.png'),
  main_logo: require('../../assets/images/main_logo.png'),
  help_image: require('../../assets/images/help_image.png'),
  current_location: require('../../assets/images/current_location.png'),
  close_icon: require('../../assets/images/close.png'),
  feedback_icon: require('../../assets/images/feedback_icon.png'),
  locate: require('../../assets/images/locate.png'),
};
