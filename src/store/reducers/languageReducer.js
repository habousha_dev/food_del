import {I18nManager} from 'react-native';
import {UPDATE_LANGUAGE} from '../actions/languageAction';
// import { saveItemObject } from '../../config/helpers';

const initial_state = {
  language: {
    rtl: I18nManager.isRTL,
    code: I18nManager.isRTL ? 'ar' : 'en',
    // 'data': products
  },
};

const languageReducer = (state = initial_state, action) => {
  switch (action.type) {
    case UPDATE_LANGUAGE:
      console.log('action.direction :', action.direction);

      if (action.direction === 'rtl') {
        I18nManager.forceRTL(true);
      } else {
        I18nManager.forceRTL(false);
      }

      const langObject = {
        rtl: !I18nManager.isRTL,
        code: I18nManager.isRTL ? 'ar' : 'en',
      };

      //AsyncStorage.setItem('@lang', langObject);
      return {...state, language: langObject};
    // return state
    default:
      return state;
  }
};

export default languageReducer;
