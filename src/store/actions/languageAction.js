export const UPDATE_LANGUAGE = 'UPDATE_LANGUAGE';

export const updateLanguage = (direction) => ({
  type: UPDATE_LANGUAGE,
  direction: direction,
});
