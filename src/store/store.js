import {createStore} from 'redux';
import reducers from './reducers/languageReducer';

const store = createStore(reducers);
// console.log('store', store.getState());

export default store;
