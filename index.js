// if (__DEV__) {
//   import('./ReactotronConfig').then(() => console.log('Reactotron Configured'));
// }

import {AppRegistry, I18nManager, YellowBox} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];
console.disableYellowBox = true;

YellowBox.ignoreWarnings([
  'VirtualizedLists should never be nested', // TODO: Remove when fixed
]);
if (!I18nManager.isRTL) {
  I18nManager.allowRTL(true);
  I18nManager.forceRTL(true);
  // RNRestart.Restart();
}

AppRegistry.registerComponent(appName, () => App);
